<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//login, reset-password, forgot-password, register
Auth::routes([
	'register'	=> false
]);

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('users', 'App\Http\Controllers\UserController', ['except' => ['edit']])->middleware('can:isAdmin');
	Route::post('users/password/{id}', 'App\Http\Controllers\UserController@updatePassword')->middleware('can:isAdmin')->name('update.password');

	Route::get('desa', 'App\Http\Controllers\DesaController@index')->name('desa');
	Route::post('desa/{id}', 'App\Http\Controllers\DesaController@update')->name('update.desa');

	Route::resource('penduduk', 'App\Http\Controllers\PendudukController');

	Route::resource('pindah-masuk', 'App\Http\Controllers\PindahMasukController');
	Route::get('pindah-masuk/print/{id}', 'App\Http\Controllers\PindahMasukController@print')->name('pindah-masuk.cetak');

	Route::resource('pindah-keluar', 'App\Http\Controllers\PindahKeluarController');
	Route::get('pindah-keluar/print/{id}', 'App\Http\Controllers\PindahKeluarController@print')->name('pindah-keluar.cetak');

	Route::resource('keluarga', 'App\Http\Controllers\KeluargaController');
	Route::post('keluarga/add-anggota/{id}', 'App\Http\Controllers\KeluargaController@tambahAnggota')->name('tambahAnggota');
	Route::delete('keluarga/remove-anggota/{id}', 'App\Http\Controllers\KeluargaController@removeAnggota')->name('removeAnggota');

	Route::get('/surat', 'App\Http\Controllers\SuratController@index')->name('surat.index');
	Route::get('/surat/create', 'App\Http\Controllers\SuratController@create')->name('surat.create');
	Route::post('/surat', 'App\Http\Controllers\SuratController@store')->name('surat.store');
	Route::get('/surat/print/{id}', 'App\Http\Controllers\SuratController@print')->name('surat.print');
	Route::delete('/surat/{id}', 'App\Http\Controllers\SuratController@destroy')->name('surat.delete');

	Route::get('/laporan', 'App\Http\Controllers\LaporanController@index')->name('laporan.index');

	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
});

Route::group(['middleware' => 'auth'], function () {
	Route::get('{page}', ['as' => 'page.index', 'uses' => 'App\Http\Controllers\PageController@index']);
});

