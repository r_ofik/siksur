@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'users'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-tools pull-right">
                            <a href="{{ route('users.index') }}" class="btn btn-info"><i class="nc-icon nc-minimal-left"></i> Kembali</a>
                        </div>
                        <h5 class="card-title">Detail Pengguna</h5>
                    </div>
                    <div class="card-body border-top">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card">
                                    <form action="{{ route('users.update', $user->id) }}" method="POST">
                                        @method('PUT')
                                        @csrf
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="">Name</label>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name', $user->name) }}" placeholder="Nama">
                                            @error('name')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="">Email</label>
                                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email', $user->email) }}" placeholder="Email">
                                            @error('email')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Hak Akses</label>
                                            <select name="role" class="custom-select @error('role') is-invalid @enderror">
                                                <option value="">-- Pilih --</option>
                                                <option value="kaur" @if(old('role', $user->role) == 'kaur') selected @endif>Kaur</option>
                                                <option value="admin" @if(old('role',$user->role) == 'admin') selected @endif>Admin</option>
                                            </select>
                                            @error('role')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <button type="reset" class="btn btn-primary"><i class="nc-icon nc-refresh-69"></i> Reset</button>
                                        <button type="submit" class="btn btn-success"><i class="nc-icon nc-tap-01"></i> Simpan</button>
                                    </div>
                                </form>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="card-title">Update Password</h5>
                                    </div>
                                    <form action="{{ route('update.password', $user->id) }}" method="post">
                                        @csrf
                                        <div class="card-body border-top">
                                            <div class="form-group">
                                                <label>Password</label>
                                                <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password">
                                                @error('password')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label>Konfirmasi Password</label>
                                                <input type="password" name="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror" placeholder="Password">
                                                @error('password_confirmation')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <button type="submit" class="btn btn-warning"><i class="nc-icon nc-tap-01"></i> Update Password</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection