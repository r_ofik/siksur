@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'users'
])

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('paper/css/sweetalert2.min.css')}}">
@endsection

@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
            @if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('status') }}.</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <div class="card-tools pull-right"><a href="{{ route('users.create') }}" class="btn btn-success"><i class="nc-icon nc-simple-add"></i> Tambah Pengguna</a></div>
                    <h5 class="card-title">Data Pengguna</h5>
                </div>
                <div class="card-body border-top table-responsive">
                    <table class="table table-hover">
                        <thead class="text-primary">
                            <tr>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Akses</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ($users->count() > 0)
                                
                                @foreach ($users as $user)
                                    <tr>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ Str::ucfirst($user->role) }}</td>
                                        <td>
                                            <a href="{{ route('users.show', $user->id) }}" title="Detail" class="btn btn-info btn-sm"><i class="nc-icon nc-zoom-split"></i></a>
                                            <button onclick="confirmDelete({{ $user->id }})" title="Hapus" class="btn btn-danger btn-sm"><i class="nc-icon nc-simple-remove"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4">
                                        <div class="alert alert-info" role="alert">
                                            Tidak ada data pengguna!
                                        </div>
                                    </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<form action="" method="post" id="delete-user">
    @method('DELETE')
    @csrf
</form>
@endsection

@push('scripts')
<script src="{{asset('paper/js/plugins/sweetalert2.all.min.js')}}"></script>
<script src="{{asset('paper/js/plugins/sweet-alerts.js')}}"></script>
<script>
    //Delete Confirmation
function confirmDelete(id) {
    Swal.fire({
          title: "Apa anda yakin?",
          text: "Anda tidak dapat mengembalikan data ini!",
          type: "warning",
          showCancelButton: true,
          cancelButtonText: "Tidak",
          confirmButtonText: "Ya, Hapus!",
          confirmButtonClass: "btn btn-danger",
          cancelButtonClass: "btn btn-primary ml-1",
          buttonsStyling: false
        }).then(function(result) {
          if (result.value) {
            $('#delete-user').attr('action', '/users/'+id);
            $('#delete-user').submit();
          }
        });
}
</script>
@endpush