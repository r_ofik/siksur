@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'desa'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col">

                @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ session('status') }}.</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                @endif

                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Identitas Desa</h5>
                    </div>
                    <form action="{{ route('update.desa', $desa->id) }}" method="POST">
                    <div class="card-body border-top">
                            @csrf
                            <div class="row">
                                <label class="col-md-3 col-form-label">Nama Desa</label>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input type="text" name="nama_desa" class="form-control @error('nama_desa') is-invalid @enderror" placeholder="Nama Desa" value="{{ old('nama_desa', $desa->nama_desa) }}">
                                        @error('nama_desa')
                                        <span class="invalid-feedback">
                                            {{ $message }}
                                        </span>
                                    @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-md-3 col-form-label">Alamat Desa</label>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input type="text" name="alamat_desa" class="form-control @error('alamat_desa') is-invalid @enderror" placeholder="Alamat Desa" value="{{ old('alamat_desa', $desa->alamat_desa) }}">
                                        @error('alamat_desa')
                                        <span class="invalid-feedback">
                                            {{ $message }}
                                        </span>
                                    @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-md-3 col-form-label">Kepala Desa</label>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input type="text" name="kades" class="form-control @error('kades') is-invalid @enderror" placeholder="Kepala Desa" value="{{ old('kades', $desa->kades) }}">
                                        @error('kades')
                                        <span class="invalid-feedback">
                                            {{ $message }}
                                        </span>
                                    @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-md-3 col-form-label">Sekertaris Desa</label>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input type="text" name="sekdes" class="form-control @error('sekdes') is-invalid @enderror" placeholder="Sekertaris Desa" value="{{ old('sekdes', $desa->sekdes) }}">
                                        @error('sekdes')
                                        <span class="invalid-feedback">
                                            {{ $message }}
                                        </span>
                                    @enderror
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success"><i class="nc-icon nc-tap-01"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection