@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'dashboard'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5 col-md-4">
                                <div class="icon-big text-center icon-warning">
                                    <i class="nc-icon nc-badge text-warning"></i>
                                </div>
                            </div>
                            <div class="col-7 col-md-8">
                                <div class="numbers">
                                    <p class="card-category">Penduduk</p>
                                    <p class="card-title">{{ $jml_penduduk }}
                                        <p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer ">
                        <hr>
                        <div class="stats">
                            <a href="{{ route('penduduk.index') }}">Lihat Semua</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5 col-md-4">
                                <div class="icon-big text-center icon-warning">
                                    <i class="nc-icon nc-credit-card text-success"></i>
                                </div>
                            </div>
                            <div class="col-7 col-md-8">
                                <div class="numbers">
                                    <p class="card-category">Keluarga</p>
                                    <p class="card-title">{{ $jml_keluarga }}
                                        <p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer ">
                        <hr>
                        <div class="stats">
                            <a href="{{ route('keluarga.index') }}">Lihat Semua</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5 col-md-4">
                                <div class="icon-big text-center icon-warning">
                                    <i class="nc-icon nc-cloud-download-93 text-danger"></i>
                                </div>
                            </div>
                            <div class="col-7 col-md-8">
                                <div class="numbers">
                                    <p class="card-category">Pindah Masuk</p>
                                    <p class="card-title">{{ $jml_masuk }}
                                        <p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer ">
                        <hr>
                        <div class="stats">
                            <a href="{{ route('pindah-masuk.index') }}">Lihat Semua</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5 col-md-4">
                                <div class="icon-big text-center icon-warning">
                                    <i class="nc-icon nc-cloud-upload-94 text-primary"></i>
                                </div>
                            </div>
                            <div class="col-7 col-md-8">
                                <div class="numbers">
                                    <p class="card-category">Pindah Keluar</p>
                                    <p class="card-title">{{ $jml_keluar }}
                                        <p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer ">
                        <hr>
                        <div class="stats">
                            <a href="{{ route('pindah-keluar.index') }}">Lihat Semua</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{-- <div class="card ">
                    <div class="card-header ">
                        <h5 class="card-title">Sirkulasi Penuduk</h5> --}}
                        {{-- <p class="card-category">24 Hours performance</p> --}}
                    {{-- </div>
                    <div class="card-body ">
                        <canvas id=chartHours width="400" height="100"></canvas>
                    </div>
                    <div class="card-footer ">
                        <hr>
                        <div class="stats">
                            Data Sirukulasi Penduduk Tahun {{ date('Y') }}
                        </div>
                    </div>
                </div> --}}
                @php
                    setlocale(LC_TIME, 'id_ID');
                    \Carbon\Carbon::setLocale('id');
                    \Carbon\Carbon::now()->formatLocalized("%A, %d %B %Y");
                @endphp
                <div class="card ">
                    <div class="card-header border-bottom">
                        <h5 class="card-title">Laporan Data Penduduk</h5>
                    </div>
                    <div class="card-body ">
                        <h6 class="text-center border-bottom">Data Penduduk Desa {{ $desa->nama_desa }} Bulan {{ \Carbon\Carbon::now()->isoFormat('MMMM Y') }}</h6>
                       
                        <table class="table table-bordered text-center">
                            <thead>
                                <tr>
                                    <th class="border" scope="col" colspan="3" rowspan="2">Penduduk Bulan Lalu</th>
                                    <th class="border" scope="col" colspan="12">Keadaan Bulan Ini</th>
                                    <th class="border" scope="col" colspan="3" rowspan="2">Jumlah s/d Bulan Ini</th>
                                </tr>
                                <tr>
                                    <th class="border" scope="col" colspan="3">Lahir</th>
                                    <th class="border" scope="col" colspan="3">Mati</th>
                                    <th class="border" scope="col" colspan="3">Pindah</th>
                                    <th class="border" scope="col" colspan="3">Datang</th>
                                </tr>
                                <tr>
                                    <th class="border">LK</th>
                                    <th class="border">PR</th>
                                    <th class="border">Jumlah</th>
                                    <th class="border">LK</th>
                                    <th class="border">PR</th>
                                    <th class="border">Jumlah</th>
                                    <th class="border">LK</th>
                                    <th class="border">PR</th>
                                    <th class="border">Jumlah</th>
                                    <th class="border">LK</th>
                                    <th class="border">PR</th>
                                    <th class="border">Jumlah</th>
                                    <th class="border">LK</th>
                                    <th class="border">PR</th>
                                    <th class="border">Jumlah</th>
                                    <th class="border">LK</th>
                                    <th class="border">PR</th>
                                    <th class="border">Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{ $rekap['lastmonth']['lk'] }}</td>
                                    <td>{{ $rekap['lastmonth']['pr'] }}</td>
                                    <td>{{ $rekap['lastmonth']['total'] }}</td>
                                    <td>{{ $rekap['thismonth']['lahir']['lk'] }}</td>
                                    <td>{{ $rekap['thismonth']['lahir']['pr'] }}</td>
                                    <td>{{ $rekap['thismonth']['lahir']['total'] }}</td>
                                    <td>{{ $rekap['thismonth']['mati']['lk'] }}</td>
                                    <td>{{ $rekap['thismonth']['mati']['pr'] }}</td>
                                    <td>{{ $rekap['thismonth']['mati']['total'] }}</td>
                                    <td>{{ $rekap['thismonth']['pindah']['lk'] }}</td>
                                    <td>{{ $rekap['thismonth']['pindah']['pr'] }}</td>
                                    <td>{{ $rekap['thismonth']['pindah']['total'] }}</td>
                                    <td>{{ $rekap['thismonth']['datang']['lk'] }}</td>
                                    <td>{{ $rekap['thismonth']['datang']['pr'] }}</td>
                                    <td>{{ $rekap['thismonth']['datang']['total'] }}</td>
                                    <td>{{ $rekap['lk'] }}</td>
                                    <td>{{ $rekap['pr'] }}</td>
                                    <td>{{ $rekap['total'] }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            // Javascript method's body can be found in assets/assets-for-demo/js/demo.js
            demo.initChartsPages();
        });
    </script>
@endpush