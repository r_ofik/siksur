@extends('layouts.app', [
    'class' => '',
    'elementActive' => $title ?? ''
])

@section('content')
@php
    setlocale(LC_TIME, 'id_ID');
\Carbon\Carbon::setLocale('id');
\Carbon\Carbon::now()->formatLocalized("%A, %d %B %Y");
@endphp
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card ">
                    <div class="card-header border-bottom">
                        <h5 class="card-title">Laporan Data Penduduk</h5>
                    </div>
                    <div class="card-body ">
                        <h6 class="text-center border-bottom">Data Penduduk Desa {{ $desa->nama_desa }} Bulan {{ \Carbon\Carbon::now()->isoFormat('MMMM Y') }}</h6>
                       
                        <table class="table table-bordered text-center">
                            <thead>
                                <tr>
                                    <th class="border" scope="col" colspan="3" rowspan="2">Penduduk Bulan Lalu</th>
                                    <th class="border" scope="col" colspan="12">Keadaan Bulan Ini</th>
                                    <th class="border" scope="col" colspan="3" rowspan="2">Jumlah s/d Bulan Ini</th>
                                </tr>
                                <tr>
                                    <th class="border" scope="col" colspan="3">Lahir</th>
                                    <th class="border" scope="col" colspan="3">Mati</th>
                                    <th class="border" scope="col" colspan="3">Pindah</th>
                                    <th class="border" scope="col" colspan="3">Datang</th>
                                </tr>
                                <tr>
                                    <th class="border">LK</th>
                                    <th class="border">PR</th>
                                    <th class="border">Jumlah</th>
                                    <th class="border">LK</th>
                                    <th class="border">PR</th>
                                    <th class="border">Jumlah</th>
                                    <th class="border">LK</th>
                                    <th class="border">PR</th>
                                    <th class="border">Jumlah</th>
                                    <th class="border">LK</th>
                                    <th class="border">PR</th>
                                    <th class="border">Jumlah</th>
                                    <th class="border">LK</th>
                                    <th class="border">PR</th>
                                    <th class="border">Jumlah</th>
                                    <th class="border">LK</th>
                                    <th class="border">PR</th>
                                    <th class="border">Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{ $rekap['lastmonth']['lk'] }}</td>
                                    <td>{{ $rekap['lastmonth']['pr'] }}</td>
                                    <td>{{ $rekap['lastmonth']['total'] }}</td>
                                    <td>{{ $rekap['thismonth']['lahir']['lk'] }}</td>
                                    <td>{{ $rekap['thismonth']['lahir']['pr'] }}</td>
                                    <td>{{ $rekap['thismonth']['lahir']['total'] }}</td>
                                    <td>{{ $rekap['thismonth']['mati']['lk'] }}</td>
                                    <td>{{ $rekap['thismonth']['mati']['pr'] }}</td>
                                    <td>{{ $rekap['thismonth']['mati']['total'] }}</td>
                                    <td>{{ $rekap['thismonth']['pindah']['lk'] }}</td>
                                    <td>{{ $rekap['thismonth']['pindah']['pr'] }}</td>
                                    <td>{{ $rekap['thismonth']['pindah']['total'] }}</td>
                                    <td>{{ $rekap['thismonth']['datang']['lk'] }}</td>
                                    <td>{{ $rekap['thismonth']['datang']['pr'] }}</td>
                                    <td>{{ $rekap['thismonth']['datang']['total'] }}</td>
                                    <td>{{ $rekap['lk'] }}</td>
                                    <td>{{ $rekap['pr'] }}</td>
                                    <td>{{ $rekap['total'] }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection