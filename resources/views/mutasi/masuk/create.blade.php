@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'masuk'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <div class="card-tools pull-right">
                            <a href="{{ route('pindah-masuk.index') }}" class="btn btn-info"><i class="nc-icon nc-minimal-left"></i> Kembali</a>
                        </div>
                        <h5 class="card-title">Pertambahan Penduduk</h5>
                    </div>
                    <form action="{{ route('pindah-masuk.store') }}" method="POST">
                        @csrf
                    <div class="card-body border-top">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>NIK <span class="text-danger">*</span></label>
                                <input type="text" class="form-control @error('nik') is-invalid @enderror" name="nik" placeholder="Nomor Induk Kependudukan" value="{{ old('nik') }}">
                                @error('nik')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label>Nama Lengkap <span class="text-danger">*</span></label>
                                <input type="text" class="form-control @error('nama') is-invalid @enderror" placeholder="Nama Lengkap" name="nama" value="{{ old('nama') }}">
                                @error('nama')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Alamat Tinggal <span class="text-danger">*</span></label>
                            <input type="text" class="form-control @error('alamat_tinggal') is-invalid @enderror" placeholder="Alamat Tinggal" name="alamat_tinggal" value="{{ old('alamat_tinggal') }}">
                            @error('alamat_tinggal')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label>Tempat Lahir <span class="text-danger">*</span></label>
                                <input type="text" class="form-control @error('tempat_lahir') is-invalid @enderror" value="{{ old('tempat_lahir') }}" name="tempat_lahir" placeholder="Tempat Lahir">
                                @error('tempat_lahir')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group col-md-4">
                                <label>Tanggal Lahir <span class="text-danger">*</span></label>
                                <input type="date" class="form-control @error('tanggal_lahir') is-invalid @enderror" name="tanggal_lahir" value="{{ old('tanggal_lahir') }}" placeholder="dd/mm/yyy">
                                @error('tanggal_lahir')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group col-md-4">
                                <label>Jenis Kelamain <span class="text-danger">*</span></label>
                                <select name="jenis_kelamin" class="form-control @error('jenis_kelamin') is-invalid @enderror">
                                  <option value="">-- Pilih --</option>
                                  <option value="L" @if(old('jenis_kelamin') == 'L') selected @endif>Laki-Laki</option>
                                  <option value="P" @if(old('jenis_kelamin') == 'P') selected @endif>Perempuan</option>
                                </select>
                                @error('jenis_kelamin')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label>Agama <span class="text-danger">*</span></label>
                                <select name="agama" class="form-control @error('agama') is-invalid @enderror">
                                  <option value="">-- Pilih --</option>
                                  <option value="Islam" @if(old('agama') == 'Islam') selected @endif>Islam</option>
                                  <option value="Kristen" @if(old('agama') == 'Kristen') selected @endif>Kristen</option>
                                  <option value="Katolik" @if(old('agama') == 'Katolik') selected @endif>Katolik</option>
                                  <option value="Hindu" @if(old('agama') == 'Hindu') selected @endif>Hindu</option>
                                  <option value="Budha" @if(old('agama') == 'Budha') selected @endif>Budha</option>
                                  <option value="Kong Hu Cu" @if(old('agama') == 'Kong Hu Cu') selected @endif>Kong Hu Cu</option>
                                  <option value="Lainnya" @if(old('agama') == 'Lainnya') selected @endif>Lainnya</option>
                                </select>
                                @error('agama')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group col-md-4">
                                <label>Golongan Darah</label>
                                <select name="gol_darah" class="form-control @error('gol_darah') is-invalid @enderror">
                                  <option value="">-- Pilih --</option>
                                  <option value="A" @if(old('gol_darah') == 'A') selected @endif>A</option>
                                  <option value="B" @if(old('gol_darah') == 'B') selected @endif>B</option>
                                  <option value="AB" @if(old('gol_darah') == 'AB') selected @endif>AB</option>
                                  <option value="O" @if(old('gol_darah') == 'O') selected @endif>O</option>
                                  <option value="--" @if(old('gol_darah') == '--') selected @endif>--</option>
                                </select>
                                @error('gol_darah')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group col-md-4">
                                <label>Status Pernikahan <span class="text-danger">*</span></label>
                                <select name="status_nikah" class="form-control @error('status_nikah') is-invalid @enderror">
                                  <option value="">-- Pilih --</option>
                                  <option value="Menikah" @if(old('status_nikah') == 'Menikah') selected @endif>Menikah</option>
                                  <option value="Belum Menikah" @if(old('status_nikah') == 'Belum Menikah') selected @endif>Belum Menikah</option>
                                  <option value="Duda atau Janda" @if(old('status_nikah') == 'Duda atau Janda') selected @endif>Duda atau Janda</option>
                                  <option value="Lainnya" @if(old('status_nikah') == 'Lainnya') selected @endif>Lainnya</option>
                                </select>
                                @error('status_nikah')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label>Pendidikan</label>
                                <input type="text" class="form-control @error('pendidikan') is-invalid @enderror" value="{{ old('pendidikan') }}" name="pendidikan" placeholder="Pendidikan">
                                @error('pendidikan')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group col-md-4">
                                <label>Pekerjaan </label>
                                <input type="text" class="form-control @error('pekerjaan') is-invalid @enderror" name="pekerjaan" value="{{ old('pekerjaan') }}" placeholder="Pekerjaan">
                                @error('pekerjaan')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group col-md-4">
                                <label>Warga Negara </label>
                                <input type="text" class="form-control @error('warga_negara') is-invalid @enderror" name="warga_negara" value="{{ old('warga_negara') }}" placeholder="Warga Negara">
                                @error('warga_negara')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label>Jenis Mutasi <span class="text-danger">*</span></label>
                                <select name="jenis_mutasi" class="form-control @error('jenis_mutasi') is-invalid @enderror" id="jenis_mutasi">
                                  <option value="">-- Pilih --</option>
                                  <option value="Lahir" @if(old('jenis_mutasi') == 'Lahir') selected @endif>Lahir</option>
                                  <option value="Pindah Masuk" @if(old('jenis_mutasi') == 'Pindah Masuk') selected @endif>Pindah Masuk</option>
                                </select>
                                @error('jenis_mutasi')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group col-md-4">
                                <label>Tanggal Kejadian <span class="text-danger">*</span></label>
                                <input type="date" class="form-control @error('tanggal_kejadian') is-invalid @enderror" name="tanggal_kejadian" value="{{ old('tanggal_kejadian') }}" placeholder="dd/mm/yyy">
                                @error('tanggal_kejadian')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row" style="display: none;" id="orang_tua">
                            <div class="form-group col-md-4">
                                <label>Ayah <sapn class="text-danger">*</sapn></label>
                                <select name="ayah" class="form-control orang-tua @error('ayah') is-invalid @enderror">
                                  <option value="">-- Pilih Ayah --</option>
                                  @foreach ($ayah as $item)
                                      <option value="{{ $item->id }}" @if(old('ayah') == $item->id) selected @endif>{{ $item->nik }} - ( {{ $item->nama }} )</option>
                                  @endforeach
                                </select>
                                @error('ayah')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group col-md-4">
                                <label>Ibu <span class="text-danger">*</span></label>
                                <select name="ibu" class="form-control orang-tua @error('ibu') is-invalid @enderror">
                                  <option value="">-- Pilih Ibu --</option>
                                  @foreach ($ibu as $item)
                                      <option value="{{ $item->id }}" @if(old('ibu') == $item->id) selected @endif>{{ $item->nik }} - ( {{ $item->nama }} )</option>
                                  @endforeach
                                </select>
                                @error('ibu')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label>Keterangan </label>
                                <input type="text" class="form-control @error('keterangan') is-invalid @enderror" name="keterangan" value="{{ old('keterangan') }}" placeholder="Keterangan">
                                @error('keterangan')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="reset" class="btn btn-primary"><i class="nc-icon nc-refresh-69"></i> Reset</button>
                        <button type="submit" class="btn btn-success"><i class="nc-icon nc-tap-01"></i> Simpan</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(()=>{
            $('#jenis_mutasi').on('change',function(){
                var type = $(this).val();
                if (type == 'Lahir') {
                    $('#orang_tua .orang-tua').removeAttr('disabled');
                    $('#orang_tua').slideDown();
                }else{
                    $('#orang_tua .orang-tua').attr('disabled', 'disabled');
                    $('#orang_tua').slideUp();
                }
            });
        });
        @if ($errors->has('ayah') OR $errors->has('ibu') OR old('jenis_mutasi') == 'Lahir')
            $('#orang_tua').show();
        @endif
    </script>
@endpush