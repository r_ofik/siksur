@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'keluar'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col">
                @if (session('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ session('error') }}.</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <div class="card-tools pull-right">
                            <a href="{{ route('pindah-keluar.index') }}" class="btn btn-info"><i class="nc-icon nc-minimal-left"></i> Kembali</a>
                        </div>
                        <h5 class="card-title">Edit Data Mutasi</h5>
                    </div>
                    <form action="{{ route('pindah-keluar.update', $mutasi->id) }}" method="POST">
                        @method('PUT')
                        @csrf
                        <div class="card-body border-top">
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label>Penduduk <span class="text-danger">*</span></label>
                                    <select name="penduduk" class="form-control @error('penduduk') is-invalid @enderror">
                                    <option value="">-- Pilih --</option>
                                    @foreach ($penduduks as $penduduk)
                                        <option value="{{ $penduduk->id }}" @if(old('penduduk', $mutasi->penduduk_id) == $penduduk->id) selected @endif>{{ $penduduk->nik }} - ( {{ $penduduk->nama }} )</option>
                                    @endforeach
                                    </select>
                                    @error('penduduk')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Jenis Mutasi <span class="text-danger">*</span></label>
                                    <select name="jenis_mutasi" class="form-control @error('jenis_mutasi') is-invalid @enderror">
                                      <option value="">-- Pilih --</option>
                                      <option value="Wafat" @if(old('jenis_mutasi', $mutasi->jenis) == 'Wafat') selected @endif>Wafat</option>
                                      <option value="Pindah Keluar" @if(old('jenis_mutasi', $mutasi->jenis) == 'Pindah Keluar') selected @endif>Pindah Keluar</option>
                                    </select>
                                    @error('jenis_mutasi')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Tanggal Kejadian <span class="text-danger">*</span></label>
                                    <input type="date" class="form-control @error('tanggal_kejadian') is-invalid @enderror" name="tanggal_kejadian" value="{{ old('tanggal_kejadian', $mutasi->tanggal) }}" placeholder="dd/mm/yyy">
                                    @error('tanggal_kejadian')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label>Keterangan </label>
                                    <input type="text" class="form-control @error('keterangan') is-invalid @enderror" name="keterangan" value="{{ old('keterangan', $mutasi->keterangan) }}" placeholder="Keterangan">
                                    @error('keterangan')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success"><i class="nc-icon nc-tap-01"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection