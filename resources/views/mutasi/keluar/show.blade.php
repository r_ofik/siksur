@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'masuk'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <div class="card-tools pull-right">
                            <a href="{{ route('pindah-keluar.index') }}" class="btn btn-info"><i class="nc-icon nc-minimal-left"></i> Kembali</a>
                        </div>
                        <h5 class="card-title">Detail Data Mutasi</h5>
                    </div>
                    <div class="card-body border-top">
                        <div class="row mb-2">
                            <div class="col-md-4">
                                Nomor Induk Kependudukan (NIK)
                            </div>
                            <div class="col-md-8">
                                : <strong>{{ $penduduk->anggota->nik }}</strong>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-4">
                                Nama Lengkap
                            </div>
                            <div class="col-md-8">
                                : <strong>{{ $penduduk->anggota->nama }}</strong>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-4">
                                Alamat
                            </div>
                            <div class="col-md-8">
                                : <strong>{{ $penduduk->anggota->alamat_tinggal }}</strong>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-4">
                                Tempat, Tanggal Lahir
                            </div>
                            <div class="col-md-8">
                                : <strong>{{ $penduduk->anggota->tmp_lahir }}, {{ date_format(date_create($penduduk->anggota->tgl_lahir), 'd-m-Y') }}</strong>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-4">
                                Jenis Kelamin
                            </div>
                            <div class="col-md-8">
                                : <strong>{{ ($penduduk->anggota->jenis_kelamin == 'L') ? 'Laki-Laki' : 'Perempuan' }}</strong>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-4">
                                Agama
                            </div>
                            <div class="col-md-8">
                                : <strong>{{ $penduduk->anggota->agama }}</strong>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-4">
                                Golongan Darah
                            </div>
                            <div class="col-md-8">
                                : <strong>{{ $penduduk->anggota->gol_darah }}</strong>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-4">
                                Status Perkawinan
                            </div>
                            <div class="col-md-8">
                                : <strong>{{ $penduduk->anggota->status_nikah }}</strong>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-4">
                                Pendidikan
                            </div>
                            <div class="col-md-8">
                                : <strong>{{ $penduduk->anggota->pendidikan }}</strong>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-4">
                                Pekerjaan
                            </div>
                            <div class="col-md-8">
                                : <strong>{{ $penduduk->anggota->pekerjaan }}</strong>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-4">
                                Kewarganegaraan
                            </div>
                            <div class="col-md-8">
                                : <strong>{{ $penduduk->anggota->warga_negara }}</strong>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-4">
                                Jenis Mutasi
                            </div>
                            <div class="col-md-8">
                                : <strong>{{ $penduduk->jenis }}</strong>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-4">
                                Tanggal Kejadian
                            </div>
                            <div class="col-md-8">
                                : <strong>{{ date_format(date_create($penduduk->tanggal), 'd-m-Y') }}</strong>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-4">
                                Keterangan
                            </div>
                            <div class="col-md-8">
                                : <strong>{{ $penduduk->keternagan }}</strong>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('pindah-masuk.edit', $penduduk->id) }}" title="Edit" class="btn btn-warning"><i class="nc-icon nc-ruler-pencil"></i> Ubah Data</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection