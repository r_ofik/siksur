@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'surat'
])

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('paper/css/sweetalert2.min.css')}}">
@endsection

@section('content')
    <div class="content">
        <div class="row">
            <div class="col">
                @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ session('status') }}.</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                @if (session('cetak_surat'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Surat Telah Dibuat.</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <div class="card-tools pull-right"><button class="btn btn-success" data-toggle="modal" data-target="#modalCreate"><i class="nc-icon nc-simple-add"></i> Buat Surat</button></div>
                        <h5 class="card-title">Data Surat</h5>
                    </div>
                    <div class="card-body border-top">
                        <table class="table table-hover">
                            <thead class="text-primary">
                                <tr>
                                    <th>#</th>
                                    <th>No. Surat</th>
                                    <th>Nama Penduduk</th>
                                    <th>Nama Surat</th>
                                    <th>Jenis Surat</th>
                                    <th>Tanggal</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($surats->count() == 0)
                                    <tr>
                                        <td colspan="7">
                                            <div class="alert alert-info">Tidak ada data.</div>
                                        </td>
                                    </tr>
                                @else
                                    @foreach ($surats as $key => $surat)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td>{{ ($surat->jenis == 'Surat Pindah' || $surat->jenis == 'Surat Domisili') ? '475/0'.$surat->no_surat.'/432.507.09'.'/'.date('Y') : '473/0'.$surat->no_surat.'/432.507.09'.'/'.date('Y') }}</td>
                                            <td>{{ $surat->penduduk->nama }}</td>
                                            <td>{{ $surat->nama }}</td>
                                            <td>{{ $surat->jenis }}</td>
                                            <td>{{ date_format(date_create($surat->tanggal), 'd-m-Y') }}</td>
                                            <td>
                                                <a href="{{ route('surat.print', $surat->id) }}" target="_bank" title="Cetak" class="btn btn-info btn-sm"><i class="nc-icon nc-paper"></i></a>
                                                {{-- <a href="" title="Edit" class="btn btn-warning btn-sm"><i class="nc-icon nc-ruler-pencil"></i></a> --}}
                                                <button onclick="confirmDelete({{ $surat->id }})" title="Hapus" class="btn btn-danger btn-sm"><i class="nc-icon nc-simple-remove"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
<div class="modal fade" id="modalCreate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalCreate">Buat Surat</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('surat.create') }}" method="GET">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Penduduk</label>
                        <select name="penduduk" class="custom-select" required>
                            <option value="">-- Pilih Penduduk --</option>
                            @foreach ($penduduks as $penduduk)
                                <option value="{{ $penduduk->id }}">{{ $penduduk->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Jenis Surat</label>
                        <select name="jenis_surat" class="custom-select" required>
                            <option value="">-- Pilih Jenis Surat --</option>
                            <option value="Surat Kelahiran">Surat Kelahiran</option>
                            <option value="Surat Kematian">Surat Kematian</option>
                            <option value="Surat Pindah">Surat Pindah</option>
                            <option value="Surat Domisili">Surat Domisili</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Nama Surat</label>
                        <input type="text" class="form-control" name="nama_surat" placeholder="Nama Surat" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Buat</button>
                </div>
            </form>
        </div>
    </div>
</div>

    <form action="" method="post" id="delete-surat">
        @method('DELETE')
        @csrf
    </form>
@endsection

@push('scripts')
<script src="{{asset('paper/js/plugins/sweetalert2.all.min.js')}}"></script>
<script src="{{asset('paper/js/plugins/sweet-alerts.js')}}"></script>
<script>
    @if (session('cetak_surat'))
    window.open("/surat/print/"+{{ session('cetak_surat') }}, '_blank').focus();
    @endif
    //Delete Confirmation
function confirmDelete(id) {
    Swal.fire({
          title: "Apa anda yakin?",
          text: "Anda tidak dapat mengembalikan data ini!",
          type: "warning",
          showCancelButton: true,
          cancelButtonText: "Tidak",
          confirmButtonText: "Ya, Hapus!",
          confirmButtonClass: "btn btn-danger",
          cancelButtonClass: "btn btn-primary ml-1",
          buttonsStyling: false
        }).then(function(result) {
          if (result.value) {
            $('#delete-surat').attr('action', '/surat/'+id);
            $('#delete-surat').submit();
          }
        });
}
</script>
@endpush