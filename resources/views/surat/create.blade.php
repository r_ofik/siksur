@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'surat'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ session('status') }}.</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif

                <div class="card">
                    <div class="card-header">
                        <div class="card-tools pull-right">
                            <a href="{{ route('surat.index') }}" class="btn btn-info"><i class="nc-icon nc-minimal-left"></i> Kembali</a>
                        </div>
                        <h5 class="card-title">{{ $nama_surat }}</h5>
                    </div>
                    <form action="{{ route('surat.store') }}" method="post">
                        <div class="card-body border-top">
                            @csrf
                            <input type="hidden" name="penduduk" value="{{ $penduduk->id }}">
                            <input type="hidden" name="jenis_surat" value="{{ $jenis_surat }}">
                            <input type="hidden" name="nama_surat" value="{{ $nama_surat }}">

                            @if ($content == 'kelahiran')
                                @include('surat.form.lahir')
                            @elseif($content == 'meninggal')
                                @include('surat.form.meninggal')
                            @elseif($content == 'pindah')
                                @include('surat.form.pindah')
                            @elseif($content == 'domisili')
                                @include('surat.form.domisili')
                            @endif

                        </div>
                        <div class="card-footer text-right">
                            <button class="btn btn-success">Cetak</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection