<h6>Identitas Penduduk:</h6>
<table class="table">
    <tr>
        <td>Nama </td>
        <td> : {{ $penduduk->nama }}</td>
    </tr>
    <tr>
        <td>Tempat, Tanggal Lahir</td>
        <td> : {{ $penduduk->tmp_lahir }}, {{ $penduduk->tgl_lahir }}</td>
    </tr>
    <tr>
        <td>
            NIK
        </td>
        <td>
            : {{ $penduduk->nik }}
        </td>
    </tr>
    <tr>
        <td>
            NO KK
        </td>
        <td>
            : {{ ($penduduk->keluarga != null) ? $penduduk->keluarga->no_kk : '' }}
        </td>
    </tr>
    <tr>
        <td>
            Kewarganegaraan
        </td>
        <td>
            : {{ $penduduk->warga_negara }}
        </td>
    </tr>
    <tr>
        <td>Jenis Kelamin</td>
        <td> : {{ ($penduduk->jenis_kelamin == 'L') ? 'Laki-Laki' : 'Perempuan' }}</td>
    </tr>
    <tr>
        <td>
            Status Penikahan
        </td>
        <td>
            : {{ $penduduk->status_nikah }}
        </td>
    </tr>
    <tr>
        <td>
            Pekerjaan
        </td>
        <td>
            : {{ $penduduk->pekerjaan }}
        </td>
    </tr>
    <tr>
        <td>Agama</td>
        <td>
            : {{ $penduduk->agama }}
        </td>
    </tr>
    <tr>
        <td>Pendidikan Terakhir</td>
        <td>
            : {{ $penduduk->pendidikan }}
        </td>
    </tr>
    <tr>
        <td>Alamat</td>
        <td>
            : {{ $penduduk->alamat_tinggal }}
        </td>
    </tr>
    <tr>
        <td>Keperluan</td>
        <td>
            <input type="text" name="keperluan" class="form-control" placeholder="Dibuat Untuk" required>
        </td>
    </tr>
</table>