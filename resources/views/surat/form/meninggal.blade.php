<h6>Identitas Penduduk:</h6>
<table class="table">
    <tr>
        <td>Nama </td>
        <td> : {{ $penduduk->nama }}</td>
    </tr>
    <tr>
        <td>Tempat, Tanggal Lahir</td>
        <td> : {{ $penduduk->tmp_lahir }}, {{ $penduduk->tgl_lahir }}</td>
    </tr>
    <tr>
        <td>Jenis Kelamin</td>
        <td> : {{ ($penduduk->jenis_kelamin == 'L') ? 'Laki-Laki' : 'Perempuan' }}</td>
    </tr>
    <tr>
        <td>
            Status Penikahan
        </td>
        <td>
            : {{ $penduduk->status_nikah }}
        </td>
    </tr>
    <tr>
        <td>
            Pekerjaan
        </td>
        <td>
            : {{ $penduduk->pekerjaan }}
        </td>
    </tr>
    <tr>
        <td>Agama</td>
        <td>
            : {{ $penduduk->agama }}
        </td>
    </tr>
    <tr>
        <td>Alamat</td>
        <td>
            : {{ $penduduk->alamat_tinggal }}
        </td>
    </tr>
    <tr>
        <td>
            Tanggal Meninggal
        </td>
        <td>
            <input type="date" name="tanggal_meninggal" class="form-control" placeholder="Tanggal Meninggal" required>
        </td>
    </tr>
    <tr>
        <td>
            Tempat Kejadian
        </td>
        <td>
            <input type="text" name="tempat_meninggal" class="form-control" placeholder="Tempat Meninggal" required>
        </td>
    </tr>
    <tr>
        <td>Keperluan</td>
        <td>
            <input type="text" name="keperluan" class="form-control" placeholder="Dibuat Untuk" required>
        </td>
    </tr>
</table>