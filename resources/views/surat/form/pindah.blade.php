<h6>Identitas Penduduk:</h6>
<table class="table">
    <tr>
        <td>Nama </td>
        <td colspan="2"> : {{ $penduduk->nama }}</td>
    </tr>
    <tr>
        <td>Tempat, Tanggal Lahir</td>
        <td colspan="2"> : {{ $penduduk->tmp_lahir }}, {{ $penduduk->tgl_lahir }}</td>
    </tr>
    <tr>
        <td>
            NIK
        </td>
        <td colspan="2">
            : {{ $penduduk->nik }}
        </td>
    </tr>
    <tr>
        <td>
            NO KK
        </td>
        <td colspan="2">
            : {{ ($penduduk->keluarga != null) ? $penduduk->keluarga->no_kk : '' }}
        </td>
    </tr>
    <tr>
        <td>
            Kewarganegaraan
        </td>
        <td colspan="2">
            : {{ $penduduk->warga_negara }}
        </td>
    </tr>
    <tr>
        <td>Jenis Kelamin</td>
        <td> : {{ ($penduduk->jenis_kelamin == 'L') ? 'Laki-Laki' : 'Perempuan' }}</td>
    </tr>
    <tr>
        <td>
            Status Penikahan
        </td>
        <td colspan="2">
            : {{ $penduduk->status_nikah }}
        </td>
    </tr>
    <tr>
        <td>
            Pekerjaan
        </td>
        <td colspan="2">
            : {{ $penduduk->pekerjaan }}
        </td>
    </tr>
    <tr>
        <td>Agama</td>
        <td colspan="2">
            : {{ $penduduk->agama }}
        </td>
    </tr>
    <tr>
        <td>Pendidikan Terakhir</td>
        <td colspan="2">
            : {{ $penduduk->pendidikan }}
        </td>
    </tr>
    <tr>
        <td>Alamat Asal</td>
        <td colspan="2">
            : {{ $penduduk->alamat_tinggal }}
        </td>
    </tr>
    <tr>
        <td>Pindah Ke</td>
        <td>
            : Desa/Keluarahan 
        </td>
        <td><input type="text" name="alamat_tujuan" class="form-control" placeholder="Desa/Kelurahan" required></td>
    </tr>
    <tr>
        <td></td>
        <td>&nbsp; Kecamatan</td>
        <td><input type="text" name="kec_tujuan" class="form-control" placeholder="Kecamatan" required></td>
    </tr>
    <tr>
        <td></td>
        <td>&nbsp; Kabupaten/Kota</td>
        <td><input type="text" name="kab_tujuan" class="form-control" placeholder="Kabupaten/Kota" required></td>
    </tr>
    <tr>
        <td></td>
        <td>&nbsp; Provinsi</td>
        <td><input type="text" name="provinsi" class="form-control" placeholder="Provinsi" required></td>
    </tr>
    <tr>
        <td></td>
        <td>&nbsp; Tanggal Pindah</td>
        <td><input type="date" name="tanggal_pindah" class="form-control" placeholder="Tanggal Pindah" required></td>
    </tr>
    <tr>
        <td>Alasan Pindah</td>
        <td colspan="2">
            <input type="text" name="alasan" class="form-control" placeholder="Alasan Pindah" required>
        </td>
    </tr>
</table>