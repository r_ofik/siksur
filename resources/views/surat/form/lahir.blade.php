<h6>Identitas Bayi:</h6>
<table class="table">
    <tr>
        <td>Nama </td>
        <td> : {{ $penduduk->nama }}</td>
    </tr>
    <tr>
        <td>Tempat, Tanggal Lahir</td>
        <td> : {{ $penduduk->tmp_lahir }}, {{ $penduduk->tgl_lahir }}</td>
    </tr>
    <tr>
        <td>Jenis Kelamin</td>
        <td> : {{ ($penduduk->jenis_kelamin == 'L') ? 'Laki-Laki' : 'Perempuan' }}</td>
    </tr>
    <tr>
        <td>
            Panjang
        </td>
        <td>
            <input type="text" name="panjang" class="form-control" placeholder="Panjang  Bayi" required>
        </td>
    </tr>
    <tr>
        <td>
            Berat Badan
        </td>
        <td>
            <input type="text" name="berat" class="form-control" placeholder="Berat  Badan" required>
        </td>
    </tr>
    <tr>
        <td>Lahir Di</td>
        <td>
            <input type="text" name="t_lahir" class="form-control" placeholder="Tempat Lahir" required>
        </td>
    </tr>
    <tr>
        <td>Anak Ke</td>
        <td>
            <input type="text" name="anak_ke" class="form-control" placeholder="Anak Ke" required>
        </td>
    </tr>
    <tr>
        <td>
            Ayah
        </td>
        <td>
            <select name="ayah" class="custom-select" required>
                <option value="">-- Pilih --</option>
                @foreach ($ayah as $penduduk)
                    <option value="{{ $penduduk->id }}">{{ $penduduk->nama }}</option>
                @endforeach
            </select>
        </td>
    </tr>
    <tr>
        <td>
            Ibu
        </td>
        <td>
            <select name="ibu" class="custom-select" required>
                <option value="">-- Pilih --</option>
                @foreach ($ibu as $penduduk)
                    <option value="{{ $penduduk->id }}">{{ $penduduk->nama }}</option>
                @endforeach
            </select>
        </td>
    </tr>
    <tr>
        <td>Keperluan</td>
        <td>
            <input type="text" name="keperluan" class="form-control" placeholder="Dibuat Untuk" required>
        </td>
    </tr>
</table>