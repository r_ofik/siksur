<p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Yang bertanda tangan dibawah ini kami Kepala Desa {{ $desa->nama_desa }} Kecamatan Pegantenan Kabupaten Pamekasan, dengan ini menerangkan :
</p>
@php
    setlocale(LC_TIME, 'id_ID');
    \Carbon\Carbon::setLocale('id');
    \Carbon\Carbon::now()->formatLocalized("%A, %d %B %Y");
@endphp
<table style="width: 100%; margin-left:20px;">
    <tr>
        <td width="40%">Nama</td>
        <td>: {{ $surat->penduduk->nama }}</td>
    </tr>
    <tr>
        <td>Jenis Kelamin</td>
        <td>: {{ ($surat->penduduk->jenis_kelamin == 'L') ? 'Laki-Laki' : 'Perempuan'}}</td>
    </tr>
    <tr>
        <td>Tempat & Tanggal Lahir</td>
        <td>: {{ $surat->penduduk->tmp_lahir }}, {{ date_format(date_create($surat->penduduk->tgl_lahir), 'd-m-Y') }}</td>
    </tr>
    <tr>
        <td>Status Kawin</td>
        <td>: {{ $surat->penduduk->status_nikah }}</td>
    </tr>
    <tr>
        <td>Agama</td>
        <td>: {{ $surat->penduduk->agama }}</td>
    </tr>
    <tr>
        <td>Pekerjaan</td>
        <td>: {{ $surat->penduduk->pekerjaan }}</td>
    </tr>
    <tr>
        <td>Alamat</td>
        <td>: {{ $surat->penduduk->alamat_tinggal }}</td>
    </tr>
</table>
<p align="center">
    <strong>MENERANGKAN</strong>
</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bahwa orang di atas telah meninggal dunia pada :</p>
<table style="width: 100%; margin-left:20px;">
    <tr>
        <td width="40%">Hari/Tanggal</td>
        <td>: {{ \Carbon\Carbon::parse($surat->tanggal)->isoFormat('dddd, DD-MMMM-Y') }}</td>
    </tr>
    <tr>
        <td>Tempat</td>
        <td>: {{ $surat->keterangan}}</td>
    </tr>
</table>
<p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Demikian surat keterangan ini kami buat dengan sebenar-benarnya dan dapat dipergunakan sebagaimana mestinya.
</p>
<p style="text-align: right;margin-right:50px;">
    Pamekasan, {{ \Carbon\Carbon::now()->isoFormat('DD MMMM Y') }}
</p>
<div style="float: right; text-align:center; width:40%;">
<p style="margin:0 0 70px 0;">Kepala Desa {{ $desa->nama_desa }}</p>
<p style="text-transform: uppercase; font-weight:bold; text-decoration:underline;">{{ $desa->kades }}</p>
</div>