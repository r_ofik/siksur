<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title ?? '' }}</title>
    <style>
        .kop, .surat{
            text-align: center;
        }
        .kop .logo{
            float: left;
            margin-left: 10px;
            margin-right: -90px;
        }
        .kop h5, .kop h2, .surat h5{
            margin:0;
            padding: 0;
        }
        .kop {
            border-bottom: 4px double #000;
        }
        .surat .title{
            margin-top: 30px;
            text-decoration: underline;
            text-transform: uppercase;
        }
        .content{
            text-align: justify;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="kop">
            <img src="{{ public_path('images/pemkab-pamekasan.png') }}" alt="logo" width="80" class="logo">
            <h5>PEMERINTAH KABUPATEN PAMEKASAN</h5>
            <h5>KECAMATAN PEGANTENAN</h5>
            <h2>DESA {{ Str::upper($desa->nama_desa) }}</h2>
            {{ $desa->alamat_desa }}
        </div>

        <div class="surat">
            <h5 class="title">{{ $surat->nama }}</h5>
            <h5>{{ ($surat->jenis == 'Surat Pindah' || $surat->jenis == 'Surat Domisili') ? '475/0'.$surat->no_surat.'/432.507.09'.'/'.date('Y') : '473/0'.$surat->no_surat.'/432.507.09'.'/'.date('Y') }}</h5>
        </div>

        <div class="content">
            @if ($surat->jenis == 'Surat Kelahiran')
                @include('surat.template.kelahiran')
            @elseif($surat->jenis == 'Surat Kematian')
                @include('surat.template.meninggal')
            @elseif($surat->jenis == 'Surat Pindah')
                @include('surat.template.pindah')
            @elseif($surat->jenis == 'Surat Domisili')
                @include('surat.template.domisili')
            @endif
        </div>
    </div>
</body>
</html>