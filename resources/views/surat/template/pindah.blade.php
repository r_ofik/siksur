<p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Yang bertanda tangan dibawah ini kami Kepala Desa {{ $desa->nama_desa }} Kecamatan Pegantenan Kabupaten Pamekasan, dengan ini menerangkan :
</p>
@php
    setlocale(LC_TIME, 'id_ID');
    \Carbon\Carbon::setLocale('id');
    \Carbon\Carbon::now()->formatLocalized("%A, %d %B %Y");
@endphp
<table style="width: 100%; margin-left:20px;">
    <tr>
        <td width="40%">1. Nama</td>
        <td>: {{ $surat->penduduk->nama }}</td>
    </tr>
    <tr>
        <td>2. Jenis Kelamin</td>
        <td>: {{ ($surat->penduduk->jenis_kelamin == 'L') ? 'Laki-Laki' : 'Perempuan'}}</td>
    </tr>
    <tr>
        <td>3. Tempat & Tanggal Lahir</td>
        <td>: {{ $surat->penduduk->tmp_lahir }}, {{ date_format(date_create($surat->penduduk->tgl_lahir), 'd-m-Y') }}</td>
    </tr>
    <tr>
        <td>4. NIK</td>
        <td>: {{ $surat->penduduk->nik }}</td>
    </tr>
    <tr>
        <td>5. Kewarganegaraan</td>
        <td>: {{ $surat->penduduk->warga_negara }}</td>
    </tr>
    <tr>
        <td>6. Agama</td>
        <td>: {{ $surat->penduduk->agama }}</td>
    </tr>
    <tr>
        <td>7. Status Kawin</td>
        <td>: {{ $surat->penduduk->status_nikah }}</td>
    </tr>
    <tr>
        <td>8. Pekerjaan</td>
        <td>: {{ $surat->penduduk->pekerjaan }}</td>
    </tr>
    <tr>
        <td>9. Pendidikan Terakhir</td>
        <td>: {{ $surat->penduduk->pendidikan }}</td>
    </tr>
    <tr>
        <td>10. Alamat Asal</td>
        <td>: {{ $surat->penduduk->alamat_tinggal }}</td>
    </tr>
    <tr>
        <td style="vertical-align: top;">11. Pindah Ke</td>
        <td>
            : Desa/Kelurahan &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{ $surat->alamat_tujuan }}
            <br>&nbsp;&nbsp;Kecamatan &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{ $surat->kecamatan_tujuan }}
            <br>&nbsp;&nbsp;Kab./Kota &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{ $surat->kab_tujuan }}
            <br>&nbsp;&nbsp;Provinsi &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{ $surat->prov_tujuan }}
            <br>&nbsp;&nbsp;Pada Tanggal &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{ date_format(date_create($surat->tanggal), 'd-m-Y') }}
        </td>
    </tr>
    <tr>
        <td>12. Alasan Pindah</td>
        <td>: {{ $surat->alasan }}</td>
    </tr>
</table>
<p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Demikian surat keterangan ini kami buat dengan sebenar-benarnya dan dapat dipergunakan sebagaimana mestinya.
</p>
<p style="text-align: right;margin-right:50px;">
    Pamekasan, {{ \Carbon\Carbon::now()->isoFormat('DD MMMM Y') }}
</p>
<div style="float: right; text-align:center; width:40%;">
<p style="margin:0 0 70px 0;">Kepala Desa {{ $desa->nama_desa }}</p>
<p style="text-transform: uppercase; font-weight:bold; text-decoration:underline;">{{ $desa->kades }}</p>
</div>