<p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Yang bertanda tangan dibawah ini kami Kepala Desa {{ $desa->nama_desa }} Kecamatan Pegantenan Kabupaten Pamekasan, dengan ini menerangkan :
</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Telah lahir anak yang bernama :</p>
<table style="width: 100%; margin-left:20px;">
    <tr>
        <td width="40%">Nama Bayi</td>
        <td>: {{ $surat->penduduk->nama }}</td>
    </tr>
    <tr>
        <td>Tanggal/Bulan/Tahun</td>
        <td>: {{ date_format(date_create($surat->penduduk->tgl_lahir), 'd/m/Y') }}</td>
    </tr>
    <tr>
        <td>Jenis Kelamin</td>
        <td>: {{ ($surat->penduduk->jenis_kelamin == 'L') ? 'Laki-Laki' : 'Perempuan'}}</td>
    </tr>
    <tr>
        <td>Panjang</td>
        <td>: {{ $surat->panjang }} cm</td>
    </tr>
    <tr>
        <td>Berat</td>
        <td>: {{ $surat->berat }} kg</td>
    </tr>
    <tr>
        <td>Lahir Di</td>
        <td>: {{ $surat->t_lahir }}</td>
    </tr>
    <tr>
        <td>Anak Ke</td>
        <td>: {{ $surat->anak_ke }}</td>
    </tr>
</table>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Adalah anak dari pasangan suami-istri :</p>
<p><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>AYAH:</u></strong></p>
<table style="width: 100%; margin-left:40px;">
    <tr>
        <td width="40%">Nama</td>
        <td>: {{ $surat->penduduk->ayah->nama }}</td>
    </tr>
    <tr>
        <td>Umur</td>
        <td>: {{ \Carbon\Carbon::parse($surat->penduduk->ayah->tgl_lahir)->diff(\Carbon\Carbon::now())->y }} Thn</td>
    </tr>
    <tr>
        <td>Pekerjaan</td>
        <td>: {{ $surat->penduduk->ayah->pekerjaan }}</td>
    </tr>
    <tr>
        <td>Alamat</td>
        <td>: {{ $surat->penduduk->ayah->alamat }}</td>
    </tr>
</table>
<p><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>IBU:</u></strong></p>
<table style="width: 100%; margin-left:40px;">
    <tr>
        <td width="40%">Nama</td>
        <td>: {{ $surat->penduduk->ibu->nama }}</td>
    </tr>
    <tr>
        <td>Umur</td>
        <td>: {{ \Carbon\Carbon::parse($surat->penduduk->ibu->tgl_lahir)->diff(\Carbon\Carbon::now())->y }} Thn</td>
    </tr>
    <tr>
        <td>Pekerjaan</td>
        <td>: {{ $surat->penduduk->ibu->pekerjaan }}</td>
    </tr>
    <tr>
        <td>Alamat</td>
        <td>: {{ $surat->penduduk->ibu->alamat }}</td>
    </tr>
</table>
<p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Surat keterangan ini dibuat untuk : <strong>{{ $surat->keperluan }}</strong>
</p>
<p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Demikian surat keterangan ini kami buat dengan sebenar-benarnya dan dapat dipergunakan sebagaimana mestinya.
</p>
<p style="text-align: right;margin-right:50px;">
    @php
        setlocale(LC_TIME, 'id_ID');
        \Carbon\Carbon::setLocale('id');
        \Carbon\Carbon::now()->formatLocalized("%A, %d %B %Y");
    @endphp
    Pamekasan, {{ \Carbon\Carbon::now()->isoFormat('DD MMMM Y') }}
</p>
<div style="float: right; text-align:center; width:40%;">
<p style="margin:0 0 70px 0;">Kepala Desa {{ $desa->nama_desa }}</p>
<p style="text-transform: uppercase; font-weight:bold; text-decoration:underline;">{{ $desa->kades }}</p>
</div>