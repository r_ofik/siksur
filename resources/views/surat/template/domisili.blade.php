<p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Yang bertanda tangan dibawah ini kami Kepala Desa {{ $desa->nama_desa }} Kecamatan Pegantenan Kabupaten Pamekasan, dengan ini menerangkan :
</p>
<table style="width: 100%; margin-left:20px;">
    <tr>
        <td width="40%">1. Nama</td>
        <td>: {{ $surat->penduduk->nama }}</td>
    </tr>
    <tr>
        <td>2. Jenis Kelamin</td>
        <td>: {{ ($surat->penduduk->jenis_kelamin == 'L') ? 'Laki-Laki' : 'Perempuan'}}</td>
    </tr>
    <tr>
        <td>3. Tempat & Tanggal Lahir</td>
        <td>: {{ $surat->penduduk->tmp_lahir }}, {{ date_format(date_create($surat->penduduk->tgl_lahir), 'd-m-Y') }}</td>
    </tr>
    <tr>
        <td>4. NIK</td>
        <td>: {{ $surat->penduduk->nik }}</td>
    </tr>
    <tr>
        <td>5. No. KK</td>
        <td>: {{ $surat->penduduk->keluarga->no_kk }}</td>
    </tr>
    <tr>
        <td>6. Kewarganegaraan</td>
        <td>: {{ $surat->penduduk->warga_negara }}</td>
    </tr>
    <tr>
        <td>7. Agama</td>
        <td>: {{ $surat->penduduk->agama }}</td>
    </tr>
    <tr>
        <td>8. Status Kawin</td>
        <td>: {{ $surat->penduduk->status_nikah }}</td>
    </tr>
    <tr>
        <td>9. Pekerjaan</td>
        <td>: {{ $surat->penduduk->pekerjaan }}</td>
    </tr>
    <tr>
        <td>10. Pendidikan Terakhir</td>
        <td>: {{ $surat->penduduk->pendidikan }}</td>
    </tr>
    <tr>
        <td>11. Alamat Asal</td>
        <td>: {{ $surat->penduduk->alamat_tinggal }}</td>
    </tr>
</table>
<p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Orang  tersebut di atas benar-benar Berdomisili/Penduduk Desa {{ $desa->nama_desa }} Kec. Pegantenan Kab. Pamekasan sejak Lahir sampai saat ini.</strong>
</p>
<p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Demikian surat keterangan ini kami buat dengan sebenar-benarnya dan untuk dipergunakan sebagaimana mestinya.
</p>
<p style="text-align: right;margin-right:50px;">
    @php
        setlocale(LC_TIME, 'id_ID');
        \Carbon\Carbon::setLocale('id');
        \Carbon\Carbon::now()->formatLocalized("%A, %d %B %Y");
    @endphp
    Pamekasan, {{ \Carbon\Carbon::now()->isoFormat('DD MMMM Y') }}
</p>
<div style="float: right; text-align:center; width:40%;">
<p style="margin:0 0 70px 0;">Kepala Desa {{ $desa->nama_desa }}</p>
<p style="text-transform: uppercase; font-weight:bold; text-decoration:underline;">{{ $desa->kades }}</p>
</div>