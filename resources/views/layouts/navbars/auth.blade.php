<div class="sidebar" data-color="white" data-active-color="danger">
    <div class="logo">
        <a href="{{ route('home') }}" class="simple-text logo-mini">
            <div class="logo-image-small">
                {{-- <i class="nc-icon nc-circle-10"></i> --}}
                <img src="{{ asset('paper') }}/img/logo-small.png">
            </div>
        </a>
        <a href="{{ route('home') }}" class="simple-text logo-normal">
            SIADES
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="{{ $elementActive == 'dashboard' ? 'active' : '' }}">
                <a href="{{ route('home') }}">
                    <i class="nc-icon nc-bank"></i>
                    <p>{{ __('Dashboard') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'penduduk' || $elementActive == 'keluarga' ? 'active' : '' }}" aria-expanded="{{ $elementActive == 'penduduk' || $elementActive == 'keluarga' ? true : false }}">
                <a data-toggle="collapse" aria-expanded="false" href="#kependudukan">
                    <i class="nc-icon nc-book-bookmark"></i>
                    <p>
                            Kependudukan
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ $elementActive == 'penduduk' || $elementActive == 'keluarga' ? 'show' : '' }}" id="kependudukan">
                    <ul class="nav">
                        <li class="{{ $elementActive == 'penduduk' ? 'active' : '' }}">
                            <a href="{{ route('penduduk.index') }}">
                                <i class="nc-icon nc-badge"></i> Penduduk
                            </a>
                        </li>
                        <li class="{{ $elementActive == 'keluarga' ? 'active' : '' }}">
                            <a href="{{ route('keluarga.index') }}">
                                <i class="nc-icon nc-credit-card"></i> Keluarga
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="{{ $elementActive == 'masuk' || $elementActive == 'keluar' ? 'active' : '' }}">
                <a data-toggle="collapse" aria-expanded="false" href="#sirkulasi">
                    <i class="nc-icon nc-refresh-69"></i>
                    <p>
                            Sirkulasi
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ $elementActive == 'masuk' || $elementActive == 'keluar' ? 'show' : '' }}"" id="sirkulasi">
                    <ul class="nav">
                        <li class="{{ $elementActive == 'masuk' ? 'active' : '' }}">
                            <a href="{{ route('pindah-masuk.index') }}">
                                <i class="nc-icon nc-calendar-60"></i>
                                Lahir / Pindah Masuk
                            </a>
                        </li>
                        <li class="{{ $elementActive == 'keluar' ? 'active' : '' }}">
                            <a href="{{ route('pindah-keluar.index') }}">
                                <i class="nc-icon nc-user-run"></i>
                                Wafat / Pindah Keluar
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="{{ $elementActive == 'surat' ? 'active' : '' }}">
                <a href="{{ route('surat.index') }}">
                    <i class="nc-icon nc-single-copy-04"></i>
                    <p>Surat</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'laporan' ? 'active' : '' }}">
                <a href="{{ route('laporan.index') }}">
                    <i class="nc-icon nc-paper"></i>
                    <p>Laporan</p>
                </a>
            </li>
            @can('isAdmin')
            <li class="{{ $elementActive == 'desa' ? 'active' : '' }}">
                <a href="{{ route('desa') }}">
                    <i class="nc-icon nc-touch-id"></i>
                    <p>Indentitas Desa</p>
                </a>
            </li>
            @endcan
            <li class="{{ $elementActive == 'profile' ? 'active' : '' }}">
                <a href="{{ route('profile.edit') }}">
                    <i class="nc-icon nc-circle-10"></i>
                    <p>Profil</p>
                </a>
            </li>
            @can('isAdmin')
            <li class="{{ $elementActive == 'users' ? 'active' : '' }}">
                <a href="{{ route('users.index') }}">
                    <i class="nc-icon nc-single-02"></i>
                    <p>Pengguna</p>
                </a>
            </li>
            @endcan
        </ul>
    </div>
</div>
