<footer class="footer footer-black  footer-white ">
    <div class="container-fluid">
        <div class="row">
            <nav class="footer-nav">
                
            </nav>
            <div class="credits ml-auto">
                <span class="copyright">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>{{ __(', dibuat dengan penuh ') }}<i class="fa fa-heart heart"></i>{{ __(' oleh ') }} Megawati
                </span>
            </div>
        </div>
    </div>
</footer>