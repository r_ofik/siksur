@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'penduduk'
])

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('paper/css/sweetalert2.min.css')}}">
@endsection

@section('content')
    <div class="content">
        <div class="row">
            <div class="col">
                @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ session('status') }}.</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <div class="card-tools pull-right"><a href="{{ route('penduduk.create') }}" class="btn btn-success"><i class="nc-icon nc-simple-add"></i> Tambah Penduduk</a></div>
                        <h5 class="card-title">Data Penduduk</h5>
                    </div>
                    <div class="card-body border-top">
                        <table class="table table-hover">
                            <thead class="text-primary">
                                <tr>
                                    <th>#</th>
                                    <th>NIK</th>
                                    <th>Nama</th>
                                    <th>Tgl Lahir</th>
                                    <th>L/P</th>
                                    <th>Pendidikan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($penduduks->count() == 0)
                                    <tr>
                                        <td colspan="7">
                                            <div class="alert alert-info">Tidak ada data penduduk.</div>
                                        </td>
                                    </tr>
                                @else
                                    @foreach ($penduduks as $key => $penduduk)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td>{{ $penduduk->nik }}</td>
                                            <td>{{ $penduduk->nama }}</td>
                                            <td>{{ date_format(date_create($penduduk->tgl_lahir), 'd-m-Y') }}</td>
                                            <td>{{ $penduduk->jenis_kelamin }}</td>
                                            <td>{{ $penduduk->pendidikan }}</td>
                                            <td>
                                                <a href="{{ route('penduduk.show', $penduduk->id) }}" title="Detail" class="btn btn-info btn-sm"><i class="nc-icon nc-zoom-split"></i></a>
                                                <a href="{{ route('penduduk.edit', $penduduk->id) }}" title="Edit" class="btn btn-warning btn-sm"><i class="nc-icon nc-ruler-pencil"></i></a>
                                                <button onclick="confirmDelete({{ $penduduk->id }})" title="Hapus" class="btn btn-danger btn-sm"><i class="nc-icon nc-simple-remove"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    <form action="" method="post" id="delete-penduduk">
        @method('DELETE')
        @csrf
    </form>
@endsection

@push('scripts')
<script src="{{asset('paper/js/plugins/sweetalert2.all.min.js')}}"></script>
<script src="{{asset('paper/js/plugins/sweet-alerts.js')}}"></script>
<script>
    //Delete Confirmation
function confirmDelete(id) {
    Swal.fire({
          title: "Apa anda yakin?",
          text: "Anda tidak dapat mengembalikan data ini!",
          type: "warning",
          showCancelButton: true,
          cancelButtonText: "Tidak",
          confirmButtonText: "Ya, Hapus!",
          confirmButtonClass: "btn btn-danger",
          cancelButtonClass: "btn btn-primary ml-1",
          buttonsStyling: false
        }).then(function(result) {
          if (result.value) {
            $('#delete-penduduk').attr('action', '/penduduk/'+id);
            $('#delete-penduduk').submit();
          }
        });
}
</script>
@endpush