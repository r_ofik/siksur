@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'keluarga'
])

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('paper/css/sweetalert2.min.css')}}">
@endsection

@section('content')
    <div class="content">
        <div class="row">
            <div class="col">
                @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ session('status') }}.</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <div class="card-tools pull-right">
                            <a href="{{ route('keluarga.index') }}" class="btn btn-info"><i class="nc-icon nc-minimal-left"></i> Kembali</a>
                        </div>
                        <h5 class="card-title">Detail Keluarga</h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label for="">No. KK</label>
                                <input type="text" class="form-control-plaintext" readonly value="{{ $keluarga->no_kk }}">
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="">Kepala Keluarga</label>
                                <input type="text" class="form-control-plaintext" readonly value="{{ $keluarga->kepala->nama }}">
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="">Ekonomi</label>
                                <input type="text" class="form-control-plaintext" readonly value="{{ $keluarga->ekonomi }}">
                            </div>
                        </div>
                        <div class="row border-top">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="card-tools pull-right">
                                            <button data-toggle="modal" data-target="#tambahAnggota" class="btn btn-sm btn-round btn-success"><i class="nc-icon nc-simple-add"></i> Tambah Anggota</a>
                                        </div>
                                        <h6 class="card-title">Anggota Keluarga</h6>
                                    </div>
                                    <div class="card-body border-top">
                                        <table class="table table-hover">
                                            <thead class="text-primary">
                                                <tr>
                                                    <th>#</th>
                                                    <th>NIK</th>
                                                    <th>Nama Lengkap</th>
                                                    <th>L/P</th>
                                                    <th>Status</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if ($keluarga->anggota->count() == 0)
                                                    <tr>
                                                        <td colspan="6">
                                                            <div class="alert alert-info" role="alert">
                                                                Tidak ada anggota!
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endif
                                                @foreach ($keluarga->anggota as $key => $anggota)
                                                    <tr>
                                                        <td>{{ $key+1 }}</td>
                                                        <td>{{ $anggota->nik }}</td>
                                                        <td>{{ $anggota->nama }}</td>
                                                        <td>{{ $anggota->jenis_kelamin }}</td>
                                                        <td>{{ $anggota->status_keluarga }}</td>
                                                        <td>
                                                            <a href="{{ route('penduduk.show', $anggota->id) }}" title="Detail" class="btn btn-info btn-sm"><i class="nc-icon nc-zoom-split"></i></a>
                                                            @if ($anggota->status_keluarga != 'Kepala Keluarga')
                                                            <button onclick="confirmDelete({{ $anggota->id }})" title="Hapus" class="btn btn-danger btn-sm"><i class="nc-icon nc-simple-remove"></i></button
                                                                
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="tambahAnggota" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Anggota</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('tambahAnggota', $keluarga->id) }}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Penduduk</label>
                            <select name="penduduk" class="custom-select @error('penduduk') is-invalid @enderror">
                                <option value="">-- Pilih --</option>
                                @foreach ($penduduks as $penduduk)
                                    <option value="{{ $penduduk->id }}" @if(old('penduduk') == $penduduk->id) selected @endif>{{ $penduduk->nik }} - {{ $penduduk->nama }}</option>
                                @endforeach
                            </select>
                            @error('penduduk')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Status</label>
                            <select name="status_keluarga" class="custom-select @error('status_keluarga') is-invalid @enderror">
                                <option value="">-- Pilih --</option>
                                <option value="Suami" @if(old('status_keluarga') == 'Suami') selected @endif>Suami</option>
                                <option value="Istri" @if(old('status_keluarga') == 'Istri') selected @endif>Istri</option>
                                <option value="Anak" @if(old('status_keluarga') == 'Anak') selected @endif>Anak</option>
                                <option value="Menantu" @if(old('status_keluarga') == 'Menantu') selected @endif>Menantu</option>
                                <option value="Cucu" @if(old('status_keluarga') == 'Cucu') selected @endif>Cucu</option>
                                <option value="Orang Tua" @if(old('status_keluarga') == 'Orang Tua') selected @endif>Orang Tua</option>
                                <option value="Famili Lain" @if(old('status_keluarga') == 'Famili Lain') selected @endif>Famili Lain</option>
                                <option value="Pembantu" @if(old('status_keluarga') == 'Pembantu') selected @endif>Pembantu</option>
                                <option value="Lainnya" @if(old('status_keluarga') == 'Lainnya') selected @endif>Lainnya</option>
                            </select>
                            @error('status_keluarga')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="nc-icon nc-simple-remove"></i>Batal</button>
                        <button type="submit" class="btn btn-success"><i class="nc-icon nc-tap-01"></i> Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <form action="" method="post" id="delete-anggota">
        @method('DELETE')
        @csrf
    </form>
@endsection

@push('scripts')
@if($errors->any())
<script>
$('#tambahAnggota').modal({show: true});
</script>
@endif
<script src="{{asset('paper/js/plugins/sweetalert2.all.min.js')}}"></script>
<script src="{{asset('paper/js/plugins/sweet-alerts.js')}}"></script>
<script>
    //Delete Confirmation
function confirmDelete(id) {
    Swal.fire({
          title: "Apa anda yakin?",
          text: "Anda akan mengeluarkan anggota keluarga!",
          type: "warning",
          showCancelButton: true,
          cancelButtonText: "Tidak",
          confirmButtonText: "Ya, Hapus!",
          confirmButtonClass: "btn btn-danger",
          cancelButtonClass: "btn btn-primary ml-1",
          buttonsStyling: false
        }).then(function(result) {
          if (result.value) {
            $('#delete-anggota').attr('action', '/keluarga/remove-anggota/'+id);
            $('#delete-anggota').submit();
          }
        });
}
</script>
@endpush