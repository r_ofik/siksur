@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'keluarga'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <div class="card-tools pull-right">
                            <a href="{{ route('keluarga.index') }}" class="btn btn-info"><i class="nc-icon nc-minimal-left"></i> Kembali</a>
                        </div>
                        <h5 class="card-title">Tambah Keluarga</h5>
                    </div>
                    <form action="{{ route('keluarga.store') }}" method="POST">
                    @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="">No. KK</label>
                                    <input type="text" class="form-control @error('no_kk') is-invalid @enderror" placeholder="Nomor Kartu Keluarga" name="no_kk" value="{{ old('no_kk') }}">
                                    @error('no_kk')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="">Kepala Keluarga</label>
                                    <select name="kepala_keluarga" class="custom-select @error('kepala_keluarga') is-invalid @enderror">
                                        <option value="">-- Pilih --</option>
                                        @foreach ($penduduks as $penduduk)
                                            <option value="{{ $penduduk->id }}" @if(old('kepala_keluarga') == $penduduk->id) selected @endif>{{ $penduduk->nama }}</option>
                                        @endforeach
                                    </select>
                                    @error('kepala_keluarga')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="">Status Ekonomi</label>
                                    <select name="ekonomi" class="custom-select @error('ekonomi') is-invalid @enderror">
                                        <option value="">-- Pilih --</option>
                                        <option value="Sangat Mampu" @if(old('ekonomi') == 'Sangat Mampu') selected @endif>Sangat Mampu</option>
                                        <option value="Cukup Mampu" @if(old('ekonomi') == 'Cukup Mampu') selected @endif>Cukup Mampu</option>
                                        <option value="Mampu" @if(old('ekonomi') == 'Mampu') selected @endif>Mampu</option>
                                        <option value="Tidak Mampu" @if(old('ekonomi') == 'Tidak Mampu') selected @endif>Tidak Mampu</option>
                                        <option value="Sangat Tidak Mampu" @if(old('ekonomi') == 'Sangat Tidak Mampu') selected @endif>Sangat Tidak Mampu</option>
                                        <option value="Lainnya" @if(old('ekonomi') == 'Lainnya') selected @endif>Lainnya</option>
                                    </select>
                                    @error('ekonomi')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="reset" class="btn btn-primary"><i class="nc-icon nc-refresh-69"></i> Reset</button>
                            <button type="submit" class="btn btn-success"><i class="nc-icon nc-tap-01"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection