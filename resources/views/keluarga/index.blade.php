@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'keluarga'
])

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('paper/css/sweetalert2.min.css')}}">
@endsection

@section('content')
    <div class="content">
        <div class="row">
            <div class="col">
                @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ session('status') }}.</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <div class="card-tools pull-right"><a href="{{ route('keluarga.create') }}" class="btn btn-success"><i class="nc-icon nc-simple-add"></i> Tambah</a></div>
                        <h5 class="card-title">Data Keluarga</h5>
                    </div>
                    <div class="card-body border-top">
                        <table class="table table-hover">
                            <thead class="text-primary">
                                <tr>
                                    <th>#</th>
                                    <th>No KK</th>
                                    <th>Kepala Keluarga</th>
                                    <th>Ekonomi</th>
                                    <th>Anggota</th>
                                    <th>Aksi</td>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($keluarga->count() > 0)
                                
                                @foreach ($keluarga as $key => $kel)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $kel->no_kk }}</td>
                                        <td>{{ $kel->kepala->nama }}</td>
                                        <td>{{ $kel->ekonomi }}</td>
                                        <td>{{ $kel->anggota->count() }}
                                        </td>
                                        <td>
                                            <a href="{{ route('keluarga.show', $kel->id) }}" title="Detail" class="btn btn-info btn-sm"><i class="nc-icon nc-zoom-split"></i></a>
                                            <a href="{{ route('keluarga.edit', $kel->id) }}" title="Edit" class="btn btn-warning btn-sm"><i class="nc-icon nc-ruler-pencil"></i></a>
                                            <button onclick="confirmDelete({{ $kel->id }})" title="Hapus" class="btn btn-danger btn-sm"><i class="nc-icon nc-simple-remove"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6">
                                        <div class="alert alert-info" role="alert">
                                            Tidak ada data keluarga!
                                        </div>
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form action="" method="post" id="delete-keluarga">
        @method('DELETE')
        @csrf
    </form>
@endsection

@push('scripts')
<script src="{{asset('paper/js/plugins/sweetalert2.all.min.js')}}"></script>
<script src="{{asset('paper/js/plugins/sweet-alerts.js')}}"></script>
<script>
    //Delete Confirmation
function confirmDelete(id) {
    Swal.fire({
          title: "Apa anda yakin?",
          text: "Anda tidak dapat mengembalikan data ini!",
          type: "warning",
          showCancelButton: true,
          cancelButtonText: "Tidak",
          confirmButtonText: "Ya, Hapus!",
          confirmButtonClass: "btn btn-danger",
          cancelButtonClass: "btn btn-primary ml-1",
          buttonsStyling: false
        }).then(function(result) {
          if (result.value) {
            $('#delete-keluarga').attr('action', '/keluarga/'+id);
            $('#delete-keluarga').submit();
          }
        });
}
</script>
@endpush