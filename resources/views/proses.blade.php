@extends('layouts.app', [
    'class' => '',
    'elementActive' => $title ?? ''
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card ">
                    <div class="card-header border-bottom">
                        <h5 class="card-title">Dalam Proses Pengembangan</h5>
                        <p class="card-category">Fitur Ini Sedang Dalam Proses Pengembangan</p>
                    </div>
                    <div class="card-body ">
                        <img src="{{ asset('images/Code_typing.svg') }}" alt="image" class="w-100">
                    </div>
                    {{-- <div class="card-footer ">
                        <hr>
                        <div class="stats">
                            <i class="fa fa-history"></i> Updated 3 minutes ago
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection