<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Keluarga extends Model
{
    use HasFactory;

    protected $table = 'keluarga';

    protected $guarded = ['id'];

    public function kepala()
    {
        return $this->hasOne('App\Models\Penduduk', 'id', 'kepala_keluarga');
    }
    
    public function anggota()
    {
        return $this->hasMany('App\Models\Penduduk','keluarga_id','id');
    }
}
