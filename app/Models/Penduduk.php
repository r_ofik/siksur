<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Penduduk extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function ayah()
    {
        return $this->hasOne('App\Models\Penduduk', 'id', 'id_ayah');
    }

    public function ibu()
    {
        return $this->hasOne('App\Models\Penduduk', 'id', 'id_ibu');
    }

    public function keluarga()
    {
        return $this->hasOne('App\Models\Keluarga', 'id', 'keluarga_id');
    }
}
