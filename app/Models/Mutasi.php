<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mutasi extends Model
{
    use HasFactory;

    protected $table = 'mutasi';

    protected $guarded = ['id'];

    public function anggota()
    {
        return $this->hasOne('App\Models\Penduduk','id','penduduk_id');
    }
}
