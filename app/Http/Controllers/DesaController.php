<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;

class DesaController extends Controller
{
    public function index()
    {

        Profile::firstOrCreate([
            'user_id'   => auth()->user()->id
        ]);

        $data['desa'] = Profile::where('user_id', auth()->user()->id)->first();

        return view('desa.index', $data);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_desa'     => 'required',
            'alamat_desa'   => 'required',
            'kades'         => 'required',
            'sekdes'        => 'required'
        ]);

        Profile::where('id', $id)->update([
            'nama_desa'     => $request->nama_desa,
            'alamat_desa'   => $request->alamat_desa,
            'kades'         => $request->kades,
            'sekdes'        => $request->sekdes
        ]);

        return back()->withStatus('Identitas desa telah diperbaharui!');
    }
}
