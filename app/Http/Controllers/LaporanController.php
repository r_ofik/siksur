<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use App\Models\Penduduk;
use Carbon\Carbon;
use App\Models\Mutasi;

class LaporanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'laporan';
        $data['desa'] = Profile::latest()->first();
        $data['rekap'] = [
            'lastmonth'  => [
                'total' => Penduduk::where('aktif', '1')->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->count(),
                'lk'    => Penduduk::where('aktif', '1')->where('jenis_kelamin', 'L')->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->count(),
                'pr'    => Penduduk::where('aktif', '1')->where('jenis_kelamin', 'P')->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->count(),
            ],
            'thismonth' => [
                'lahir' => [
                    'total' => Mutasi::where('jenis', 'Lahir')->whereMonth('created_at', '=', Carbon::now()->month)->count(),
                    'lk'    => Mutasi::where('jenis', 'Lahir')->whereHas('anggota', function($query){return $query->where('jenis_kelamin', 'L');})->whereMonth('created_at', '=', Carbon::now()->month)->count(),
                    'pr'    => Mutasi::where('jenis', 'Lahir')->whereHas('anggota', function($query){return $query->where('jenis_kelamin', 'P');})->whereMonth('created_at', '=', Carbon::now()->month)->count()
                ],
                'mati' => [
                    'total' => Mutasi::where('jenis', 'Wafat')->whereMonth('created_at', '=', Carbon::now()->month)->count(),
                    'lk'    => Mutasi::where('jenis', 'Wafat')->whereHas('anggota', function($query){return $query->where('jenis_kelamin', 'L');})->whereMonth('created_at', '=', Carbon::now()->month)->count(),
                    'pr'    => Mutasi::where('jenis', 'Wafat')->whereHas('anggota', function($query){return $query->where('jenis_kelamin', 'P');})->whereMonth('created_at', '=', Carbon::now()->month)->count()
                ],
                'pindah' => [
                    'total' => Mutasi::where('jenis', 'Pindah Keluar')->whereMonth('created_at', '=', Carbon::now()->month)->count(),
                    'lk'    => Mutasi::where('jenis', 'Pindah Keluar')->whereHas('anggota', function($query){return $query->where('jenis_kelamin', 'L');})->whereMonth('created_at', '=', Carbon::now()->month)->count(),
                    'pr'    => Mutasi::where('jenis', 'Pindah Keluar')->whereHas('anggota', function($query){return $query->where('jenis_kelamin', 'P');})->whereMonth('created_at', '=', Carbon::now()->month)->count()
                ],
                'datang' => [
                    'total' => Mutasi::where('jenis', 'Pindah Masuk')->whereMonth('created_at', '=', Carbon::now()->month)->count(),
                    'lk'    => Mutasi::where('jenis', 'Pindah Masuk')->whereHas('anggota', function($query){return $query->where('jenis_kelamin', 'L');})->whereMonth('created_at', '=', Carbon::now()->month)->count(),
                    'pr'    => Mutasi::where('jenis', 'Pindah Masuk')->whereHas('anggota', function($query){return $query->where('jenis_kelamin', 'P');})->whereMonth('created_at', '=', Carbon::now()->month)->count()
                ]
            ],
            'total' => Penduduk::where('aktif', '1')->count(),
            'lk'    => Penduduk::where('aktif', '1')->where('jenis_kelamin', 'L')->count(),
            'pr'    => Penduduk::where('aktif', '1')->where('jenis_kelamin', 'P')->count(),
        ];

        return view('laporan.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
