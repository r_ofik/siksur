<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mutasi;
use App\Models\Penduduk;

class PindahKeluarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['penduduks'] = Mutasi::where('jenis', 'Wafat')->orWhere('jenis', 'Pindah Keluar')->latest()->get();

        return view('mutasi.keluar.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['penduduks'] = Penduduk::where('aktif', '1')->latest()->get();

        return view('mutasi.keluar.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'penduduk'          => 'required',
            'jenis_mutasi'      => 'required',
            'tanggal_kejadian'  => 'required'
        ]);

        $penduduk = Penduduk::where('id', $request->penduduk)->update([
            'aktif' => '0'
        ]);

        if ($penduduk) {
            Mutasi::create([
                'penduduk_id'   => $request->penduduk,
                'tanggal'       => $request->tanggal_kejadian,
                'jenis'         => $request->jenis_mutasi,
                'keternagan'    => $request->keterangan
            ]);
        }

        return redirect('pindah-keluar')->with('status', 'Data baru telah disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['penduduk']   = Mutasi::where('id', $id)->first();

        return view('mutasi.keluar.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['penduduks'] = Penduduk::latest()->get();
        $data['mutasi']     = Mutasi::where('id', $id)->first();

        return view('mutasi.keluar.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'penduduk'          => 'required',
            'jenis_mutasi'      => 'required',
            'tanggal_kejadian'  => 'required'
        ]);

        $mutasi = Mutasi::where('id',$id)->first();
        $old_id = $mutasi->penduduk_id;

        if ($mutasi->penduduk_id != $request->penduduk) {
            Penduduk::where('id', $mutasi->penduduk_id)->update([
                'aktif' => '1'
            ]);
            Penduduk::where('id', $request->penduduk)->update([
                'aktif' => '0'
            ]);
        }

        Mutasi::where('id', $id)->update([
            'penduduk_id'   => $request->penduduk,
            'jenis'         => $request->jenis_mutasi,
            'tanggal'       => $request->tanggal_kejadian,
            'keterangan'    => $request->keterangan
        ]);

        if (Mutasi::where('penduduk_id', $old_id)->where('jenis', 'Wafat')->get()->count() > 0 OR Mutasi::where('penduduk_id', $old_id)->where('jenis', 'Pindah Keluar')->get()->count() > 0) {
            Penduduk::where('id', $old_id)->update([
                'aktif' => '0'
            ]);
        }else{
            Penduduk::where('id', $old_id)->update([
                'aktif' => '1'
            ]);
        }

        return redirect('pindah-keluar')->with('status', 'Data telah diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mutasi = Mutasi::find($id);

        Penduduk::where('id', $mutasi->penduduk_id)->update([
            'aktif' => '1'
        ]);

        $mutasi->delete();

        return redirect('pindah-keluar')->with('status', 'Data telah dihapus!');
    }

    public function print(Request $request, $id)
    {
        # code...
    }
}
