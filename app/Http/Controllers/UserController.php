<?php

namespace App\Http\Controllers;

use App\Models\User;
// use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param  \App\Models\User  $model
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $data['users'] = User::where('id', '!=', auth()->user()->id)->get();
        return view('users.index', $data);
    }

    public function create()
    {
        return view('users.create');
    }

    public function show($id)
    {
        $data['user'] = User::where('id',$id)->first();
        return view('users.detail', $data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'  => 'required',
            'email' => 'required|unique:users|email',
            'password'  => 'required|confirmed',
            'password_confirmation' => 'required',
            'role'  => 'required'
        ]);

        User::create([
            'name'  => $request->name,
            'email' => $request->email,
            'password'  => \Hash::make($request->password),
            'role'  => $request->role,
            'email_verified_at' => Carbon::now()
        ]);

        return redirect('users')->with('status', 'Pengguna baru telah dibuat!');
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name'  => 'required',
            'email' => 'required|unique:users,email,'.$id.'|email',
            'role'  => 'required'
        ]);

        User::where('id', $id)->update([
            'name'  => $request->name,
            'email' => $request->email,
            'role'  => $request->role
        ]);

        return redirect('users')->with('status', 'Data pengguna telah diubah!');
    }

    public function destroy($id)
    {
        User::find($id)->delete();

        return redirect('users')->with('status', 'Pengguna telah dihapus!');
    }

    public function updatePassword(Request $request, $id)
    {
        $request->validate([
            'password'  => 'required|confirmed',
            'password_confirmation' => 'required'
        ]);

        User::where('id', $id)->update([
            'password'  => \Hash::make($request->password)
        ]);

        return redirect('users')->with('status', 'Password Telah Diubah!');
    }
}
