<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Penduduk;

class PendudukController extends Controller
{
    /**
     * Untuk Menampilkan halaman awal penduduk
     */
    public function index()
    {
        $data['penduduks'] = Penduduk::where('aktif', '1')->latest()->get();

        return view('penduduk.index', $data);
    }

    /**
     * Untuk menampilkan tampilan tambah penduduk
     * 
     */
    public function create()
    {
        return view('penduduk.create');
    }

    /**
     * Proses Menambah data penduduk baru
     *
     * 
     */
    public function store(Request $request)
    {
        $request->validate([
            'nik'           => 'required|numeric||digits:16|unique:penduduks',
            'nama'          => 'required',
            'alamat_tinggal'    => 'required',
            'tempat_lahir'  => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'agama'         => 'required',
            'status_nikah'  => 'required'
        ]);

        Penduduk::create([
            'nik'   => $request->nik,
            'nama'  => $request->nama,
            'alamat_tinggal'    => $request->alamat_tinggal,
            'tmp_lahir'     => $request->tempat_lahir,
            'tgl_lahir'     => $request->tanggal_lahir,
            'jenis_kelamin' => $request->jenis_kelamin,
            'gol_darah'     => $request->gol_darah,
            'agama'         => $request->agama,
            'status_nikah'  => $request->status_nikah,
            'pendidikan'    => $request->pendidikan,
            'pekerjaan'     => $request->pekerjaan,
            'warga_negara'  => $request->warga_negara
        ]);

        return redirect('penduduk')->with('status', 'Penduduk baru telah disimpan!');
    }

    /**
     * Untuk menampilkan detail penduduk
     *
     * 
     */
    public function show($id)
    {
        $data['penduduk'] = Penduduk::where('id', $id)->first();

        return view('penduduk.show', $data);
    }

    /**
     * Untuk tampilan edit penduduk
     */
    public function edit($id)
    {
        $data['penduduk'] = Penduduk::where('id', $id)->first();

        return view('penduduk.edit', $data);
    }

    /**
     * Proses Update data penduduk
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nik'           => 'required|numeric||digits:16|unique:penduduks,nik,'.$id,
            'nama'          => 'required',
            'alamat_tinggal'    => 'required',
            'tempat_lahir'  => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'agama'         => 'required',
            'status_nikah'  => 'required'
        ]);

        Penduduk::where('id', $id)->update([
            'nik'           => $request->nik,
            'nama'          => $request->nama,
            'alamat_tinggal'    => $request->alamat_tinggal,
            'tmp_lahir'     => $request->tempat_lahir,
            'tgl_lahir'     => $request->tanggal_lahir,
            'jenis_kelamin' => $request->jenis_kelamin,
            'gol_darah'     => $request->gol_darah,
            'agama'         => $request->agama,
            'status_nikah'  => $request->status_nikah,
            'pendidikan'    => $request->pendidikan,
            'pekerjaan'     => $request->pekerjaan,
            'warga_negara'  => $request->warga_negara
        ]);

        return redirect('penduduk')->with('status', 'Data penduduk telah diubah!');
    }

    /**
     * Hapus data penduduk
     */
    public function destroy($id)
    {
        Penduduk::where('id', $id)->delete();

        return redirect('penduduk')->with('status', 'Data berhasil dihapus!');
    }
}
