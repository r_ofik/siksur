<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mutasi;
use App\Models\Penduduk;

class PindahMasukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['penduduks'] = Mutasi::where('jenis', 'Lahir')->orWhere('jenis', 'Pindah Masuk')->latest()->get();

        return view('mutasi.masuk.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['ayah'] = Penduduk::where('aktif', '1')->where('jenis_kelamin', 'L')->latest()->get();
        $data['ibu'] = Penduduk::where('aktif', '1')->where('jenis_kelamin', 'P')->latest()->get();

        return view('mutasi.masuk.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nik'           => 'required|numeric||digits:16|unique:penduduks',
            'nama'          => 'required',
            'alamat_tinggal'    => 'required',
            'tempat_lahir'  => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'agama'         => 'required',
            'status_nikah'  => 'required',
            'jenis_mutasi'  => 'required',
            'tanggal_kejadian'  => 'required',
            'ayah'          => ($request->jenis_mutasi == 'Lahir') ? 'required' : '',
            'ibu'           => ($request->jenis_mutasi == 'Lahir') ? 'required' : ''
        ]);

        $penduduk = Penduduk::create([
            'nik'   => $request->nik,
            'nama'  => $request->nama,
            'alamat_tinggal'    => $request->alamat_tinggal,
            'tmp_lahir'     => $request->tempat_lahir,
            'tgl_lahir'     => $request->tanggal_lahir,
            'jenis_kelamin' => $request->jenis_kelamin,
            'gol_darah'     => $request->gol_darah,
            'agama'         => $request->agama,
            'status_nikah'  => $request->status_nikah,
            'pendidikan'    => $request->pendidikan,
            'pekerjaan'     => $request->pekerjaan,
            'warga_negara'  => $request->warga_negara,
            'id_ayah'       => $request->ayah,
            'id_ibu'        => $request->ibu
        ]);

        if ($penduduk) {
            Mutasi::create([
                'penduduk_id'   => $penduduk->id,
                'jenis'         => $request->jenis_mutasi,
                'tanggal'       => $request->tanggal_kejadian,
                'keterangan'    => $request->keterangan
            ]);
        }

        return redirect('pindah-masuk')->with('status', 'Data baru telah disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['penduduk'] = Mutasi::where('id', $id)->first();

        return view('mutasi.masuk.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['penduduk'] = Mutasi::where('id', $id)->first();
        $data['ayah'] = Penduduk::where('id', '!=', $data['penduduk']->penduduk_id)->where('aktif', '1')->where('jenis_kelamin', 'L')->latest()->get();
        $data['ibu'] = Penduduk::where('id', '!=', $data['penduduk']->penduduk_id)->where('aktif', '1')->where('jenis_kelamin', 'P')->latest()->get();

        return view('mutasi.masuk.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'nik'           => 'required|numeric||digits:16|unique:penduduks,nik,'.$request->penduduk_id,
            'nama'          => 'required',
            'alamat_tinggal'    => 'required',
            'tempat_lahir'  => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'agama'         => 'required',
            'status_nikah'  => 'required',
            'jenis_mutasi'  => 'required',
            'tanggal_kejadian'  => 'required',
            'ayah'          => ($request->jenis_mutasi == 'Lahir') ? 'required' : '',
            'ibu'           => ($request->jenis_mutasi == 'Lahir') ? 'required' : ''
        ]);
        

        $penduduk = Penduduk::where('id', $request->penduduk_id)->update([
            'nik'   => $request->nik,
            'nama'  => $request->nama,
            'alamat_tinggal'    => $request->alamat_tinggal,
            'tmp_lahir'     => $request->tempat_lahir,
            'tgl_lahir'     => $request->tanggal_lahir,
            'jenis_kelamin' => $request->jenis_kelamin,
            'gol_darah'     => $request->gol_darah,
            'agama'         => $request->agama,
            'status_nikah'  => $request->status_nikah,
            'pendidikan'    => $request->pendidikan,
            'pekerjaan'     => $request->pekerjaan,
            'warga_negara'  => $request->warga_negara,
            'id_ayah'       => ($request->jenis_mutasi == 'Lahir') ? $request->ayah : null,
            'id_ibu'        => ($request->jenis_mutasi == 'Lahir') ? $request->ibu : null,
        ]);

        if ($penduduk) {
            Mutasi::where('id', $id)->update([
                'jenis'         => $request->jenis_mutasi,
                'tanggal'       => $request->tanggal_kejadian,
                'keterangan'    => $request->keterangan
            ]);
        }

        return redirect('pindah-masuk')->with('status', 'Data mutasi telah diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mutasi = Mutasi::find($id);

        Penduduk::where('id', $mutasi->penduduk_id)->delete();
        $mutasi->delete();

        return redirect('pindah-masuk')->with('status', 'Data telah dihapus!');
    }

    public function print(Request $request, $id)
    {
        # code...
    }
}
