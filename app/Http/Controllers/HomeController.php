<?php

namespace App\Http\Controllers;
use App\Models\Penduduk;
use App\Models\Keluarga;
use App\Models\Mutasi;
use App\Models\Profile;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {

        $data['desa'] = Profile::latest()->first();
        $data['jml_penduduk'] = Penduduk::where('aktif', '1')->count();
        $data['jml_keluarga'] = Keluarga::count();
        $data['jml_masuk']  = Mutasi::where('jenis', 'Pindah Masuk')->orWhere('jenis', 'Lahir')->count();
        $data['jml_keluar']  = Mutasi::where('jenis', 'Pindah Keluar')->orWhere('jenis', 'Wafat')->count();

        $data['rekap'] = [
            'lastmonth'  => [
                'total' => Penduduk::where('aktif', '1')->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->count(),
                'lk'    => Penduduk::where('aktif', '1')->where('jenis_kelamin', 'L')->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->count(),
                'pr'    => Penduduk::where('aktif', '1')->where('jenis_kelamin', 'P')->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->count(),
            ],
            'thismonth' => [
                'lahir' => [
                    'total' => Mutasi::where('jenis', 'Lahir')->whereMonth('created_at', '=', Carbon::now()->month)->count(),
                    'lk'    => Mutasi::where('jenis', 'Lahir')->whereHas('anggota', function($query){return $query->where('jenis_kelamin', 'L');})->whereMonth('created_at', '=', Carbon::now()->month)->count(),
                    'pr'    => Mutasi::where('jenis', 'Lahir')->whereHas('anggota', function($query){return $query->where('jenis_kelamin', 'P');})->whereMonth('created_at', '=', Carbon::now()->month)->count()
                ],
                'mati' => [
                    'total' => Mutasi::where('jenis', 'Wafat')->whereMonth('created_at', '=', Carbon::now()->month)->count(),
                    'lk'    => Mutasi::where('jenis', 'Wafat')->whereHas('anggota', function($query){return $query->where('jenis_kelamin', 'L');})->whereMonth('created_at', '=', Carbon::now()->month)->count(),
                    'pr'    => Mutasi::where('jenis', 'Wafat')->whereHas('anggota', function($query){return $query->where('jenis_kelamin', 'P');})->whereMonth('created_at', '=', Carbon::now()->month)->count()
                ],
                'pindah' => [
                    'total' => Mutasi::where('jenis', 'Pindah Keluar')->whereMonth('created_at', '=', Carbon::now()->month)->count(),
                    'lk'    => Mutasi::where('jenis', 'Pindah Keluar')->whereHas('anggota', function($query){return $query->where('jenis_kelamin', 'L');})->whereMonth('created_at', '=', Carbon::now()->month)->count(),
                    'pr'    => Mutasi::where('jenis', 'Pindah Keluar')->whereHas('anggota', function($query){return $query->where('jenis_kelamin', 'P');})->whereMonth('created_at', '=', Carbon::now()->month)->count()
                ],
                'datang' => [
                    'total' => Mutasi::where('jenis', 'Pindah Masuk')->whereMonth('created_at', '=', Carbon::now()->month)->count(),
                    'lk'    => Mutasi::where('jenis', 'Pindah Masuk')->whereHas('anggota', function($query){return $query->where('jenis_kelamin', 'L');})->whereMonth('created_at', '=', Carbon::now()->month)->count(),
                    'pr'    => Mutasi::where('jenis', 'Pindah Masuk')->whereHas('anggota', function($query){return $query->where('jenis_kelamin', 'P');})->whereMonth('created_at', '=', Carbon::now()->month)->count()
                ]
            ],
            'total' => Penduduk::where('aktif', '1')->count(),
            'lk'    => Penduduk::where('aktif', '1')->where('jenis_kelamin', 'L')->count(),
            'pr'    => Penduduk::where('aktif', '1')->where('jenis_kelamin', 'P')->count(),
        ];
        return view('dashboard', $data);
    }
}
