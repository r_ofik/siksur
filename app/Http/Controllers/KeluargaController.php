<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Keluarga;
use App\Models\Penduduk;

class KeluargaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['keluarga'] = Keluarga::latest()->get();

        return view('keluarga.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['penduduks'] = Penduduk::where('aktif', '1')->where('keluarga_id', null)->get();

        return view('keluarga.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'no_kk'     => 'required|numeric|digits:16|unique:keluarga',
            'kepala_keluarga'   => 'required',
            'ekonomi'       => 'required'
        ]);

        $save_keluarga = Keluarga::create([
            'no_kk' => $request->no_kk,
            'kepala_keluarga'   => $request->kepala_keluarga,
            'ekonomi'   => $request->ekonomi
        ]);

        if ($save_keluarga) {
            Penduduk::where('id', $request->kepala_keluarga)->update([
                'keluarga_id'   => $save_keluarga->id,
                'status_keluarga'   => 'Kepala Keluarga'
            ]);
        }

        return redirect('keluarga')->with('status', 'Data keluarga telah ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['keluarga']   = Keluarga::where('id', $id)->with('kepala')->first();
        $data['penduduks']  = $data['penduduks'] = Penduduk::where('aktif', '1')->where('keluarga_id', null)->get();

        return view('keluarga.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function tambahAnggota(Request $request, $id)
    {
        $request->validate([
            'penduduk'  => 'required',
            'status_keluarga'   => 'required'
        ]);

        Penduduk::where('id', $request->penduduk)->update([
            'keluarga_id'   => $id,
            'status_keluarga'   => $request->status_keluarga
        ]);

        return redirect()->back()->with('status', 'Anggota keluarga telah ditambahkan!');
    }

    public function removeAnggota($id)
    {
        Penduduk::where('id', $id)->update([
            'keluarga_id'   => null,
            'status_keluarga'   => null
        ]);

        return redirect()->back()->with('status', 'Anggota keluarga telah dihapus!');
    }

    public function edit($id)
    {
        $data['keluarga'] = Keluarga::where('id', $id)->first();
        $data['penduduks'] = Penduduk::where('aktif', '1')->where('keluarga_id', null)->get();

        return view('keluarga.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'no_kk'     => 'required|numeric|digits:16|unique:keluarga,no_kk,'.$id,
            'kepala_keluarga'   => 'required',
            'ekonomi'       => 'required'
        ]);

        $keluarga = Keluarga::where('id', $id)->first();

        if ($keluarga->kepala_keluarga != $request->kepala_keluarga) {
            Penduduk::where('id', $keluarga->kepala_keluarga)->update([
                'keluarga_id'   => null,
                'status_keluarga' => null
            ]);

            Penduduk::where('id', $request->kepala_keluarga)->update([
                'keluarga_id'   => $id,
                'status_keluarga' => 'Kepala Keluarga'
            ]);
        }else{
            Penduduk::where('id', $request->kepala_keluarga)->update([
                'keluarga_id'   => $id,
                'status_keluarga' => 'Kepala Keluarga'
            ]);
        }

        Keluarga::where('id', $id)->update([
            'no_kk' => $request->no_kk,
            'kepala_keluarga' => $request->kepala_keluarga,
            'ekonomi'   => $request->ekonomi
        ]);

        return redirect('keluarga')->with('status', 'Data keluarga telah diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Penduduk::where('keluarga_id', $id)->update([
            'keluarga_id'   => null,
            'status_keluarga'   => null
        ]);

        Keluarga::where('id', $id)->delete();

        return redirect('keluarga')->with('status', 'Keluarga telah dihapus!');
    }
}
