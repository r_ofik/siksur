<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Surat;
use App\Models\Penduduk;
use PDF;
use Carbon\Carbon;
use App\Models\Profile;

class SuratController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'surat';
        $data['penduduks'] = Penduduk::where('aktif', '1')->latest()->get();
        $data['surats'] = Surat::latest()->get();

        return view('surat.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'penduduk'      => 'required',
            'jenis_surat'   => 'required',
            'nama_surat'    => 'required'
        ]);

        $data['penduduk'] = Penduduk::where('id', $request->penduduk)->first();
        $data['nama_surat'] = $request->nama_surat;
        $data['jenis_surat'] = $request->jenis_surat;

        if ($request->jenis_surat == 'Surat Kelahiran') {
            $data['ayah'] = Penduduk::where('aktif', '1')->where('jenis_kelamin', 'L')->where('id', '!=', $request->penduduk)->latest()->get();
            $data['ibu'] = Penduduk::where('aktif', '1')->where('jenis_kelamin', 'P')->where('id', '!=', $request->penduduk)->latest()->get();
            $data['content'] = 'kelahiran';
        }elseif ($request->jenis_surat == 'Surat Kematian') {
            $data['content'] = 'meninggal';
        }elseif ($request->jenis_surat == 'Surat Pindah') {
            $data['content'] = 'pindah';
        }elseif ($request->jenis_surat == 'Surat Domisili') {
            $data['content'] = 'domisili';
        }
        
        return view('surat.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'penduduk'  => 'required',
            'jenis_surat'   => 'required'
        ]);

        if ($request->jenis_surat == 'Surat Kelahiran') {

            $store = Surat::create([
                'penduduk_id'   => $request->penduduk,
                'jenis'         => $request->jenis_surat,
                'no_surat'      => Surat::latest()->value('no_surat')+1,
                'nama'          => $request->nama_surat,
                'tanggal'       => Carbon::now(),
                'panjang'       => $request->panjang,
                'berat'         => $request->berat,
                't_lahir'       => $request->t_lahir,
                'anak_ke'       => $request->anak_ke,
                'keperluan'     => $request->keperluan
            ]);

            if ($store) {
                Penduduk::where('id', $request->penduduk)->update([
                    'id_ayah'   => $request->ayah,
                    'id_ibu'    => $request->ibu
                ]);
            }

        }elseif ($request->jenis_surat == 'Surat Kematian') {
            $store = Surat::create([
                'penduduk_id'   => $request->penduduk,
                'jenis'         => $request->jenis_surat,
                'no_surat'      => Surat::latest()->value('no_surat')+1,
                'nama'          => $request->nama_surat,
                'tanggal'       => $request->tanggal_meninggal,
                'keterangan'    => $request->tempat_meninggal,
                'keperluan'     => $request->keperluan
            ]);
        }elseif ($request->jenis_surat == 'Surat Pindah') {
            $store = Surat::create([
                'penduduk_id'   => $request->penduduk,
                'jenis'         => $request->jenis_surat,
                'no_surat'      => Surat::latest()->value('no_surat')+1,
                'nama'          => $request->nama_surat,
                'alamat_tujuan' => $request->alamat_tujuan,
                'kecamatan_tujuan'  => $request->kec_tujuan,
                'kab_tujuan'        => $request->kab_tujuan,
                'prov_tujuan'       => $request->provinsi,
                'alasan'            => $request->alasan,
                'tanggal'           => $request->tanggal_pindah
            ]);
        }elseif ($request->jenis_surat == 'Surat Domisili') {
            $store = Surat::create([
                'penduduk_id'   => $request->penduduk,
                'jenis'         => $request->jenis_surat,
                'no_surat'      => Surat::latest()->value('no_surat')+1,
                'nama'          => $request->nama_surat,
                'tanggal'       => Carbon::now(),
                'keperluan'     => $request->keperluan
            ]);
        }

        return redirect('surat')->with('cetak_surat', $store->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Surat::where('id', $id)->delete();

        return redirect('surat')->with('status', 'Data telah dihapus!');
    }

    public function print($id)
    {
        $surat = Surat::where('id', $id)->first();
        $data['surat'] = $surat;
        $data['title'] = $surat->jenis;
        $data['no_surat'] = '475/01/432.507.09/'.date('Y');
        // $data['penduduk'] = Penduduk::where('id', $surat->penduduk_id)->first();
        $data['desa'] = Profile::latest()->first();

        $pdf = PDF::loadview('surat.template.index', $data);

        return $pdf->stream($surat->nama);
    }
}
