<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLainLainToSuratTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surat', function (Blueprint $table) {
            $table->integer('panjang')->nullable();
            $table->integer('berat')->nullable();
            $table->string('t_lahir')->nullable();
            $table->integer('anak_ke')->nullable();
            $table->string('alamat_tujuan')->nullable();
            $table->string('kecamatan_tujuan')->nullable();
            $table->string('kab_tujuan')->nullable();
            $table->string('prov_tujuan')->nullable();
            $table->string('alasan')->nullable();
            $table->string('keperluan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surat', function (Blueprint $table) {
            
        });
    }
}
